package com.sofipa.sifiso.app.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sofipa.sifiso.app.models.Presupuesto;

public interface IPresupuestoDAOR extends CrudRepository<Presupuesto, Long> {

}
