package com.sofipa.sifiso.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sofipa.sifiso.app.models.CuentaModel;
import com.sofipa.sifiso.app.repositories.ICuentaDAO;

@Service
public class CuentaServiceImpl implements ICuentaService{

    @Autowired
    private ICuentaDAO cuentaDao;
    
    @Override
    public CuentaModel getCuenta(Long id_cuenta) {
        return cuentaDao.findById(id_cuenta).get();
    }

    @Override
    public List<CuentaModel> getCuentas() {
        return (List<CuentaModel>)cuentaDao.findAll();
    }

    @Override
    public void insertCuenta(CuentaModel cuenta) {
        cuentaDao.save(cuenta);
    }

    @Override
    public void updateCuenta(CuentaModel cuenta, Long id) {
        cuentaDao.findById(id).ifPresent((x)->{
            cuentaDao.save(x);
        });
    }

    @Override
    public void deleteCuenta(Long id_cuenta) {
        cuentaDao.deleteById(id_cuenta);
    }
    
}
