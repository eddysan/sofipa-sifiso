package com.sofipa.sifiso.app.services;

import java.util.List;

import com.sofipa.sifiso.app.models.CuentaModel;


public interface ICuentaService {
    public CuentaModel getCuenta(Long id_cuenta);
    public List<CuentaModel> getCuentas();
    public void insertCuenta(CuentaModel cuenta); 
    public void updateCuenta (CuentaModel cuenta, Long id);
    public void deleteCuenta (Long id_cuenta);
}
