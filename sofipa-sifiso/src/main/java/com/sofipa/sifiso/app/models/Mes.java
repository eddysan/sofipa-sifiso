package com.sofipa.sifiso.app.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tabla_mes")
public class Mes implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_mes")
	private long id_mes;
	private String nombre;
	
	public Mes(long id_mes, String nombre) {
		super();
		this.id_mes = id_mes;
		this.nombre = nombre;
	}

	public Mes() {
		super();
	}

	public long getId_mes() {
		return id_mes;
	}

	public void setId_mes(long id_mes) {
		this.id_mes = id_mes;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
