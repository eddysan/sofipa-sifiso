package com.sofipa.sifiso.app.services;

import java.util.List;

import com.sofipa.sifiso.app.models.Region;

public interface IRegionService {
	
	public List<Region> getRegiones();
	
}
