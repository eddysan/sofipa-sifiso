package com.sofipa.sifiso.app.services;

import java.util.List;

import com.sofipa.sifiso.app.models.Grupo;


public interface IGrupoService {
	List<Grupo> getCuentasAgrupadas();
}
