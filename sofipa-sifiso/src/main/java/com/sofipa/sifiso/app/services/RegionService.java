package com.sofipa.sifiso.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sofipa.sifiso.app.models.Region;
import com.sofipa.sifiso.app.repositories.IRegionDAO;

@Service
public class RegionService implements IRegionService {
	
	@Autowired
	private IRegionDAO regiones;

	@Override
	public List<Region> getRegiones() {
		// TODO Auto-generated method stub
		return regiones.getRegiones();
	}
	
}
