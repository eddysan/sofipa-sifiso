package com.sofipa.sifiso.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sofipa.sifiso.app.models.Prorrateo;
import com.sofipa.sifiso.app.services.IProrrateoService;

@RestController
public class ProrrateoController {
	
	
	@Autowired
	private IProrrateoService pr;
	
	public class ProrrateoAux{
		
		int id;
		int personal;
		Double cartera;
		Double eprc;
		
		
		
		public Double getEprc() {
			return eprc;
		}
		public void setEprc(Double eprc) {
			this.eprc = eprc;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getPersonal() {
			return personal;
		}
		public void setPersonal(int personal) {
			this.personal = personal;
		}
		public Double getCartera() {
			return cartera;
		}
		public void setCartera(Double cartera) {
			this.cartera = cartera;
		}
		
		public ProrrateoAux(String nombre, int id, int personal, Double cartera, Double eprc) {
			super();
			this.id = id;
			this.personal = personal;
			this.cartera = cartera;
			this.eprc = eprc;
		}
		
		
		
	}
	
	
	@PostMapping("/set/prorrateo/{mes}/{anio}")
	public boolean setProrrateo(@PathVariable("mes") int mes, @PathVariable("anio") int anio, @RequestBody String datos) {
		ProrrateoAux [] response = new Gson().fromJson(datos, ProrrateoAux[].class);
		return pr.insertDatosProrrateo(mes, anio, response);
	}
	
	@GetMapping("/get/prorrateo/{mes}/{anio}")
	public List<Prorrateo> getProrrateo(@PathVariable("mes") int mes, @PathVariable("anio") int anio){
		return pr.getProrrateo(mes, anio);
	
	}
	
	@GetMapping("/get/prorrateo/cartera/{mes}/{anio}")
	public Double getTotalCartera(@PathVariable("mes") int mes, @PathVariable("anio") int anio) {
		return pr.getTotalCartera(mes, anio);
	};
	
	@GetMapping("/get/prorrateo/personal/{mes}/{anio}")
	public Integer getTotalPersonal(@PathVariable("mes") int mes, @PathVariable("anio") int anio) {
		return pr.getTotalPersonal(mes, anio);
	};
	
	@GetMapping("/get/prorrateo/cartera/{mes}/{anio}/sucursal/{sucursal}")
	public Double getCartera(@PathVariable("mes") int mes, @PathVariable("anio") int anio, @PathVariable("sucursal") int sucursal) {
		return pr.getCartera(mes, anio, sucursal);
	};
	
	
	@GetMapping("/get/prorrateo/personal/{mes}/{anio}/sucursal/{sucursal}")
	public int getPersonal(@PathVariable("mes") int mes, @PathVariable("anio") int anio, @PathVariable("sucursal") int sucursal) {
		return pr.getPersonal(mes, anio, sucursal);
	};
	
	@GetMapping("/get/prorrateo/eprc/{mes}/{anio}/{sucursal}")
	public Double getEPRC(@PathVariable("mes") int mes, @PathVariable("anio") int anio, @PathVariable("sucursal") int sucursal) {
		return pr.getEPRC(mes, anio, sucursal);
	}
	
	@GetMapping("/get/prorrateo/eprc/consolidado/{mes}/{anio}")
	public Double getEprcConsolidado(@PathVariable("mes") int mes, @PathVariable("anio") int anio){
		return pr.getEprcConsolidado(mes, anio);
	}
	
	@DeleteMapping("/delete/prorrateo/{anio}/{mes}")
	public void deleteProrrateo(@PathVariable("anio") int anio,@PathVariable("mes") int mes){
		this.pr.deleteProrrateo(anio, mes);
	}
	
}
