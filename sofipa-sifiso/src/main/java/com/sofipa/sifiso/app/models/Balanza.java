package com.sofipa.sifiso.app.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tabla_balanza")
public class Balanza implements Serializable{
private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_balanza")
    private long id_balanza;
    
    private int sucursal;
    private int anio;
    private int mes;    
    private Double saldo_inicial;
    private Double cargo;
    private Double abono;
    private Double saldo_final;
    
    @ManyToOne
    @JoinColumn(name="cuenta", nullable = false)
    private CuentaModel cuenta;
	
    public Balanza(long id_balanza, int sucursal, int anio, int mes, CuentaModel cuenta, Double saldo_inicial, Double cargo,
			Double abono, Double saldo_final) {
		super();
		this.id_balanza = id_balanza;
		this.sucursal = sucursal;
		this.anio = anio;
		this.mes = mes;
		this.saldo_inicial = saldo_inicial;
		this.cargo = cargo;
		this.abono = abono;
		this.saldo_final = saldo_final;
		this.cuenta = cuenta;
	}
    
    public Balanza() {
    	super();
    }

	public long getId_balanza() {
		return id_balanza;
	}

	public void setId_balanza(long id_balanza) {
		this.id_balanza = id_balanza;
	}

	public int getSucursal() {
		return sucursal;
	}

	public void setSucursal(int sucursal) {
		this.sucursal = sucursal;
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public CuentaModel getCuenta() {
		return this.cuenta;
	}

	public void setCuenta(CuentaModel cuenta) {
		this.cuenta = cuenta;
	}

	public Double getSaldo_inicial() {
		return saldo_inicial;
	}

	public void setSaldo_inicial(Double saldo_inicial) {
		this.saldo_inicial = saldo_inicial;
	}

	public Double getCargo() {
		return cargo;
	}

	public void setCargo(Double cargo) {
		this.cargo = cargo;
	}

	public Double getAbono() {
		return abono;
	}

	public void setAbono(Double abono) {
		this.abono = abono;
	}

	public Double getSaldo_final() {
		return saldo_final;
	}

	public void setSaldo_final(Double saldo_final) {
		this.saldo_final = saldo_final;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
