package com.sofipa.sifiso.app.repositories;


import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import com.sofipa.sifiso.app.models.Balanza;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

public interface IBalanzaDAOR extends Repository<Balanza, Long>{
	
	//Sucursal consolidado y cuenta ingresos por intereses
	@Query("FROM Balanza b where b.sucursal = 58 and b.anio = :anio and b.mes = :mes and b.cuenta = 641")
	Optional<Balanza> findMovIngresosPorIntereses(@Param("anio") int anio, @Param("mes") int mes);
	
	//Sucursal consolidado y cuenta gastos por intereses
	@Query("FROM Balanza b where b.sucursal = 58 and b.anio = :anio and b.mes = :mes and b.cuenta = 725")
	Optional<Balanza> findMovGastosPorIntereses(@Param("anio") int anio, @Param("mes") int mes);
	
	
	@Query("SELECT -(b.saldo_final - b.saldo_inicial) / 1000000 FROM Balanza b WHERE b.sucursal = 58 and b.anio = :anio and b.cuenta = 641")
	List<Double> getListIngresosAnio(@Param("anio") int anio);
	
	@Query("SELECT (b.saldo_final - b.saldo_inicial) / 1000000 FROM Balanza b WHERE b.sucursal = 58 and b.anio = :anio and b.cuenta = 725")
	List<Double> getListGastosAnio(@Param("anio") int anio);
	
	@Query("SELECT b.saldo_final - b.saldo_inicial FROM Balanza b WHERE b.sucursal= :sucursal and b.anio= :anio and b.mes = :mes and b.cuenta.clave_cuenta = :cuenta")
	Double getBalanzaParaCaratula(@Param("sucursal") int sucursal, @Param("anio") int anio, @Param("mes") int mes, @Param("cuenta") String cuenta);
	
	@Query("SELECT b.abono From Balanza b WHERE b.sucursal= 58 and b.anio= :anio and b.mes = :mes and b.cuenta.clave_cuenta = 41001020000")
	Double getBalanzaParaCaratula2(@Param("anio") int anio, @Param("mes") int mes);

	@Query("FROM Balanza b WHERE b.mes = :mes and b.anio = :anio and (cuenta = 641 or cuenta = 725 or cuenta = 1545)")
	List<Balanza> findDataProrrateo(@Param("mes")int mes, @Param("anio") int anio);
	
	@Query("SELECT sum(b.saldo_final - b.saldo_inicial) From Balanza b Inner Join Sucursal s On b.sucursal = s.id Where "
			+ "b.mes = :mes and b.anio = :anio and cuenta = 725 and s.estatus = 'A' and b.sucursal != 58")
	Double getTotalGastosPorInteres(@Param("mes") int mes, @Param("anio") int anio);
	
	
	@Query("delete from Balanza b where b.anio = :anio and b.mes= :mes and b.sucursal= :sucursal")
	@Modifying
    @Transactional
	void deleteBalanza(@Param("anio") int anio, @Param("mes") int mes, @Param("sucursal") int sucursal);
	
}
