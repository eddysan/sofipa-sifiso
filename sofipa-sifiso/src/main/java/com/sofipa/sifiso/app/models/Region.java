package com.sofipa.sifiso.app.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.io.Serializable;

@Entity
@Table(name="tabla_region")
public class Region implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	private String nombre_region;
	private String estatus;
	
	public Region(long id, String nombre_region, String estatus) {
		super();
		this.id = id;
		this.nombre_region = nombre_region;
		this.estatus = estatus;
	}
	
	public Region() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre_region() {
		return nombre_region;
	}

	public void setNombre_region(String nombre_region) {
		this.nombre_region = nombre_region;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
