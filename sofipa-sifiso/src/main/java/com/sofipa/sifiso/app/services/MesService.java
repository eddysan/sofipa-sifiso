package com.sofipa.sifiso.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sofipa.sifiso.app.models.Mes;
import com.sofipa.sifiso.app.repositories.IMesDAO;

@Service
public class MesService implements IMesService{

    @Autowired
	private IMesDAO mes;
	
	@Override
	public List<Mes> getMeses() {
		List <Mes> meses = (List<Mes>) mes.findAll();
		if(meses.isEmpty()) {
			return null;
		}else {
			return meses;
		}
	}
	
	
}
