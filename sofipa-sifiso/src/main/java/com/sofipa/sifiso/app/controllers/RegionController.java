package com.sofipa.sifiso.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.sofipa.sifiso.app.models.Region;
import com.sofipa.sifiso.app.models.Sucursal;
import com.sofipa.sifiso.app.services.IRegionService;
import com.sofipa.sifiso.app.services.ISucursalService;

@RestController
public class RegionController {
	
	@Autowired
	private IRegionService regiones;
	
	@Autowired
	private ISucursalService sucursales;
	
	@GetMapping("/region/regiones")
	public List<Region> getRegiones(){
		return regiones.getRegiones();
	}
	
	@GetMapping("/region/sucursales/{region}")
	public List<Sucursal> getSucursalesRegion(@PathVariable("region")long region){
		
		return sucursales.getSucursalesRegion(region);
	}
}
