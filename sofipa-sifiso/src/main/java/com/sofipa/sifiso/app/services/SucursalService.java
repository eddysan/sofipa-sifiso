package com.sofipa.sifiso.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sofipa.sifiso.app.models.Sucursal;
import com.sofipa.sifiso.app.repositories.ISucursalDAO;

@Service
public class SucursalService implements ISucursalService{

	@Autowired
	ISucursalDAO sucursal;
	
	@Override
	public List<Sucursal> getSucursales() {
		return sucursal.getSucursales();
	}

	@Override
	public List<Sucursal> getSucursalesRegion(long region) {
		// TODO Auto-generated method stub
		return sucursal.getSucursalesRegion(region);
	}
}
