package com.sofipa.sifiso.app.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.sofipa.sifiso.app.models.EstadoDeResultados;
import com.sofipa.sifiso.app.services.IEstadoDeResultadosService;

@RestController
public class EstadoDeResultadosController {
	
	@Autowired
	IEstadoDeResultadosService estado;
	
	@GetMapping("/get/edo/{sucursal}/{mes}/{anio}")
	public ArrayList<EstadoDeResultados> getEstadoResult(@PathVariable("sucursal")int sucursal, @PathVariable("mes")int mes, 
			@PathVariable("anio")int anio) {
		ArrayList<EstadoDeResultados> estado = new ArrayList<>();
		estado.add(this.estado.getEstadoResultados(anio-1, 12, sucursal));
		for(int i = 1; i<= mes; i++) {
			estado.add(this.estado.getEstadoResultados(anio, i, sucursal));
		}
		return estado;
	}
}
