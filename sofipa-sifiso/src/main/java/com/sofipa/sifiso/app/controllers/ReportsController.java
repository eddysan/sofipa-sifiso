package com.sofipa.sifiso.app.controllers;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.sofipa.sifiso.app.models.Mes;
import com.sofipa.sifiso.app.models.ObjectReport;
import com.sofipa.sifiso.app.models.Sucursal;
import com.sofipa.sifiso.app.services.MesService;
import com.sofipa.sifiso.app.services.ReportService;
import com.sofipa.sifiso.app.services.SucursalService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;


@RestController
public class ReportsController {
	
	@Autowired
	private ReportService rs;
	
	@Autowired
	private SucursalService sucursal;
	
	@Autowired
	private MesService meses;
	
	@Autowired
	private BalanzaController bal;
	
	@GetMapping("/")
	 public ModelAndView home() {
	  ModelAndView model = new ModelAndView();

	  model.setViewName("home");
	  return model;
	 }
	
	@GetMapping("/get/report/{sucursal}/{mes}/{anio}")
	public void exportSucursal(@PathVariable("sucursal") int sucursal,
			@PathVariable("anio") int anio,
			@PathVariable("mes") int mes,
			ModelAndView model, HttpServletResponse response)throws IOException, JRException{
		/*Buscar la sucursal*/
		Sucursal suc = this.sucursal.getSucursales().stream()
				.filter(suct -> suct.getId() == sucursal).findFirst().get();
		/*Buscar el mes*/
		Mes mess = this.meses.getMeses().stream()
				.filter(mest -> mest.getId_mes() == mes).findFirst().get();
		/*Generar reporte*/
		JasperPrint jasperPrint = null;
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename=\"users.pdf\""));
		OutputStream out = response.getOutputStream();
		List<ObjectReport> listaresultados = new ArrayList<>();
		listaresultados = bal.getCaratulaReport(sucursal, anio, mes);
		List<ObjectReport> listaindicadores = new ArrayList<>();
		listaindicadores = bal.getListaIndicadores(sucursal, anio, mes);
		List<ObjectReport> resultados = new ArrayList<>();
		resultados = bal.getEresultReport(sucursal, anio, mes);
		
		jasperPrint = rs.exportPDF(suc.getNombre(),anio, mess.getNombre(),listaresultados, listaindicadores,resultados);
		JasperExportManager.exportReportToPdfStream(jasperPrint, out);
	}
}















