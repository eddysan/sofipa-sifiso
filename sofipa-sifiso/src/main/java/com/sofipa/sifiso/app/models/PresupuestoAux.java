package com.sofipa.sifiso.app.models;

import java.util.ArrayList;
import java.util.List;

public class PresupuestoAux {
	
	public class GCuenta{
		public String nombre;
		public Double monto;
		
		public GCuenta(String nombre, Double monto) {
			super();
			this.nombre = nombre;
			this.monto = monto;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public Double getMonto() {
			return monto;
		}

		public void setMonto(Double monto) {
			this.monto = monto;
		}	
	}
	
	private int anio;
	private int mes;
	private int sucursal;
	private List<GCuenta> cuentas =  new ArrayList<GCuenta>();
	
	public PresupuestoAux(int anio, int mes, int sucursal, List<GCuenta> cuentas) {
		super();
		this.anio = anio;
		this.mes = mes;
		this.sucursal = sucursal;
		this.cuentas = cuentas;
	}
	public int getAnio() {
		return anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public int getSucursal() {
		return sucursal;
	}
	public void setSucursal(int sucursal) {
		this.sucursal = sucursal;
	}
	public List<GCuenta> getCuentas() {
		return cuentas;
	}
	public void setCuentas(List<GCuenta> cuentas) {
		this.cuentas = cuentas;
	}	
}
