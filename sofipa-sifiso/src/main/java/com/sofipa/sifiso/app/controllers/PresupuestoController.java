package com.sofipa.sifiso.app.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sofipa.sifiso.app.models.Grupo;
import com.sofipa.sifiso.app.models.Presupuesto;
import com.sofipa.sifiso.app.models.PresupuestoAux;
import com.sofipa.sifiso.app.services.IGrupoService;
import com.sofipa.sifiso.app.services.IPresupuestoService;
import com.sofipa.sifiso.app.services.ISucursalService;


@RestController
public class PresupuestoController {
	
	@Autowired
	IPresupuestoService presupuestoS;
	
	@Autowired
	IGrupoService grupoS;
	
	@Autowired
	ISucursalService ss;
	
	
	@GetMapping("/get/presupuesto/{anio}/{mes}/{sucursal}/{grupo}")
	List<Presupuesto> getPresupuesto (@PathVariable("anio") int anio, 
			@PathVariable("mes") int mes, 
			@PathVariable("sucursal") int sucursal,
			@PathVariable("grupo") long grupo){
		return presupuestoS.getPresupuesto(anio, mes, sucursal, grupo);
	}
	
	@GetMapping("/get/presupuesto/grupocuentas")
	List<Grupo> getCuentasAgrupadas(){
		return grupoS.getCuentasAgrupadas();
	}
	
	@PostMapping("/set/presupuesto/{mes}/{anio}")
	public boolean setPresupuesto(@PathVariable("mes") int mes, @PathVariable("anio") int anio, @RequestBody String params) {
		PresupuestoAux [] response = new Gson().fromJson(params, PresupuestoAux[].class);
		PresupuestoAux pr = response[1];
		List<Presupuesto> aux = presupuestoS.getPresupuesto(anio, mes, pr.getSucursal());
		System.out.println(aux.size());
		if(aux.size() != 0) {
			return false;
		}
		for(PresupuestoAux p : response) {
			presupuestoS.setPresupuesto(p);
		}
		return true;
	}
	
}
