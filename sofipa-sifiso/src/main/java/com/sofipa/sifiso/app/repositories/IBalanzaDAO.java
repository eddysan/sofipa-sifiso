package com.sofipa.sifiso.app.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sofipa.sifiso.app.models.Balanza;

public interface IBalanzaDAO extends CrudRepository<Balanza, Long>{
//public interface IBalanzaDAO extends Repository<Balanza, Long>{
}
