package com.sofipa.sifiso.app.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tabla_presupuesto")
public class Presupuesto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_presupuesto")
	private long id_presupuesto;
	
	
	private int anio;
	private int mes;
	private int sucursal;
	private Double monto;
	
	@ManyToOne
	@JoinColumn(name="cuenta", nullable = false)
	private GrupoCuenta cuenta;
	
	
	
	public Presupuesto(GrupoCuenta cuenta, long id_presupuesto, int mes, int anio, int sucursal, Double monto) {
		super();
		this.cuenta = cuenta;
		this.id_presupuesto = id_presupuesto;
		this.mes = mes;
		this.anio = anio;
		this.sucursal = sucursal;
		this.monto = monto;
	}
	public Presupuesto() {
		super();
	}
	public GrupoCuenta getCuenta() {
		return cuenta;
	}
	public void setCuenta(GrupoCuenta cuenta) {
		this.cuenta = cuenta;
	}
	public long getId_presupuesto() {
		return id_presupuesto;
	}
	public void setId_presupuesto(long id_presupuesto) {
		this.id_presupuesto = id_presupuesto;
	}
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public int getAnio() {
		return anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
	public int getSucursal() {
		return sucursal;
	}
	public void setSucursal(int sucursal) {
		this.sucursal = sucursal;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
}
