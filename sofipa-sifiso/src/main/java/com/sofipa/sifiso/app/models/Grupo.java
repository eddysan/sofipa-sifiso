package com.sofipa.sifiso.app.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tabla_grupo")
public class Grupo implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_grupo")
	private long id_grupo;
	private String nombre_grupo;
	
	public Grupo(long id_grupo, String nombre_grupo) {
		super();
		this.id_grupo = id_grupo;
		this.nombre_grupo = nombre_grupo;
	}

	public Grupo() {
		super();
	}

	public long getId_grupo() {
		return id_grupo;
	}

	public void setId_grupo(long id_grupo) {
		this.id_grupo = id_grupo;
	}

	public String getNombre_grupo() {
		return nombre_grupo;
	}

	public void setNombre_grupo(String nombre_grupo) {
		this.nombre_grupo = nombre_grupo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
