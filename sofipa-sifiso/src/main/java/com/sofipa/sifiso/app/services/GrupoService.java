package com.sofipa.sifiso.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sofipa.sifiso.app.models.Grupo;
import com.sofipa.sifiso.app.repositories.IGrupoDAO;

@Service
public class GrupoService implements IGrupoService{
	
	@Autowired
	IGrupoDAO grupo;
	
	@Override
	public List<Grupo> getCuentasAgrupadas() {
		// TODO Auto-generated method stub
		return grupo.getCuentasAgrupadas();
	}

}
