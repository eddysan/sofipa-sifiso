/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sofipa.sifiso.app.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sofipa.sifiso.app.models.CuentaModel;

public interface ICuentaDAO extends CrudRepository<CuentaModel, Long>{
    
}
