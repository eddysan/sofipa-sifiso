package com.sofipa.sifiso.app.controllers;



import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.sofipa.sifiso.app.models.Balanza;
import com.sofipa.sifiso.app.models.Caratula;
import com.sofipa.sifiso.app.models.Mes;
import com.sofipa.sifiso.app.models.ObjectReport;
import com.sofipa.sifiso.app.models.Sucursal;
import com.sofipa.sifiso.app.services.IBalanzaService;
import com.sofipa.sifiso.app.services.IProrrateoService;
import com.sofipa.sifiso.app.services.ISucursalService;
import com.sofipa.sifiso.app.services.MesService;




@RestController
public class BalanzaController {
	
	
	@Autowired
	IBalanzaService BalanzaS;
	
	@Autowired
	IProrrateoService pr;
	
	@Autowired
	private ISucursalService s;
	
	@Autowired
	private MesService meses;
	
	@GetMapping("/get/balanza/{anio}/{mes}/{sucursal}")
	public List<Balanza> getBalanza(@PathVariable("anio") int anio, @PathVariable("mes") int mes, @PathVariable("sucursal") int sucursal){
		List<Balanza> balanza = BalanzaS.getBalanza(sucursal, anio, mes);
		if(balanza.isEmpty()) {
			return null;
			
		}else{
			return balanza;
			
		}
	}
	
	@PostMapping("/set/balanza/{mes}/{anio}/{sucursal}")
	public boolean setBalanza(@PathVariable("mes") int mes,
			@PathVariable("anio") int anio, 
			@PathVariable("sucursal") int sucursal,
			@RequestBody String params) throws JsonParseException, IOException{
		
		return BalanzaS.setNewBalanza(anio, mes, sucursal, params);
		
	}
	
	/*@GetMapping("/get/dashboard/ingresos/{mes}/{anio}")
	public Double getIngresosPorIntereses(@PathVariable("mes")int mes, @PathVariable("anio") int anio) {
		return BalanzaS.getIngresosPorIntereses(mes, anio);
	}*/
	
	@GetMapping("/get/dashboard/gastos/{mes}/{anio}")
	public Double getGastosPorIntereses(@PathVariable("mes")int mes, @PathVariable("anio") int anio) {
		return BalanzaS.getGastosPorIntereses(anio, mes);
	}
	
	@GetMapping("/get/dashboard/ingresos/{anio}")
	public List<Double> getIngresosAnio(@PathVariable("anio") int anio){
		return BalanzaS.getIngresosAnio(anio);
	}
	
	@GetMapping("/get/dashboard/gastos/{anio}")
	public List<Double> getGastosAnio(@PathVariable("anio") int anio){
		return BalanzaS.getGastosAnio(anio);
	}
	
	@GetMapping("/get/balanza/caratula/{sucursal}/{anio}/{mes}/{cuenta}")
	public  Double getBalnzaCaratula(@PathVariable("sucursal") int sucursal, @PathVariable("anio") int anio, @PathVariable("mes") int mes, @PathVariable("cuenta") String cuenta){
		return BalanzaS.getBalanzaPorCaratula(sucursal, anio, mes, cuenta);
	}
	
	@GetMapping("/get/balanza/inv/{anio}/{mes}")
	public Double getBalanzaCaratula2(@PathVariable("anio") int anio, @PathVariable("mes") int mes) {
		return BalanzaS.getBalanzaPorCaratula2(anio, mes);
	}
	
	@GetMapping("/get/data/prorrateo/{anio}/{mes}")
	public List<Balanza> findDataProrrateo(@PathVariable("mes") int mes,@PathVariable("anio") int anio){
		return BalanzaS.findDataProrrateo(mes, anio);
	};
	
	@GetMapping("/get/gastosinteres/{mes}/{anio}")
	public Double getTotoalGastosInteres(@PathVariable("mes") int mes,@PathVariable("anio") int anio) {
		return BalanzaS.getTotalGastosInteres(mes, anio);
	}
	
	public static final String[] CLAVES_CUENTA = {"410","510","420","520","430", "530",  "440", "450", "540","460","470", "450070000000001"};

	private HashMap<String, Caratula> result;


	@GetMapping("/get/caratula/sucursal/{mes}/{anio}/{sucursal}")
	public Caratula getCaratula(@PathVariable("sucursal") int sucursal, @PathVariable("anio") int anio, @PathVariable("mes") int mes) {
		
		Double  totalCartera 			= (pr.getTotalCartera(mes, anio) == null) 					?  	0 : pr.getTotalCartera(mes, anio); 
		Integer totalPersonal 			= (pr.getTotalPersonal(mes, anio) == null) 					? 	0 : pr.getTotalPersonal(mes, anio);
		Integer personalOficina 		= (pr.getPersonal(mes, anio, sucursal) == null) 			? 	0 : pr.getPersonal(mes, anio, sucursal);
		Double  carteraOficina 			= (pr.getCartera(mes, anio, sucursal) == null) 				?  	0 : pr.getCartera(mes, anio, sucursal);
		Double  totalGastosPorInteres	= (this.getTotoalGastosInteres(mes, anio) == null) 			?	0 : this.getTotoalGastosInteres(mes, anio);
		Double  gastoCas 			 	= (this.getBalnzaCaratula(57, anio, mes, "540") == null) 	?	0 : this.getBalnzaCaratula(57, anio, mes, "540");
		
		Mes mess = this.meses.getMeses().stream()
				.filter(mest -> mest.getId_mes() == mes).findFirst().get();
		
		
		Caratula miCaratula = new Caratula();
		
		miCaratula.setMes(mess.getNombre());
		
		//Ingreso por interes
		miCaratula.setIngresos_intereses((this.getBalnzaCaratula(sucursal, anio, mes, "410") == null) ? 0 : this.getBalnzaCaratula(sucursal, anio, mes, "410") * -1);
		
		//ingresos derivados de inversiones
		if(sucursal == 57 || sucursal == 58) {
			Double idi = this.getBalanzaCaratula2(anio, mes);
			idi = (idi == null) ? 0 : idi;
			miCaratula.setIngresos_derivados_inversiones(idi);
		}else {
			miCaratula.setIngresos_derivados_inversiones(0.0);
		}
		
		
		//Gastos por interes
		Double t_510 = this.getBalnzaCaratula(sucursal, anio, mes, "510");
		t_510 = (t_510 == null)? 0: t_510;
		Double gastos_prorrateados = (sucursal != 58) ? (totalGastosPorInteres/totalCartera) * carteraOficina: t_510;
		gastos_prorrateados = (gastos_prorrateados == null) ? 0: gastos_prorrateados;
		gastos_prorrateados = (Double.isNaN(gastos_prorrateados)) ? 0: gastos_prorrateados;
		System.out.println("Gastos prorrateados: " + gastos_prorrateados);
		miCaratula.setGastos_intereses(gastos_prorrateados);
		
		//Pocicion monetaria
		Double pmn = this.getBalnzaCaratula(sucursal, anio, mes, "420");
		pmn = (pmn==null) ? 0: pmn;
		miCaratula.setResultado_pocicion_monetaria_neto(pmn);
		
		//Margen Financiero
		Double mf = miCaratula.getIngresos_intereses() - miCaratula.getGastos_intereses() + pmn;
		miCaratula.setMargen_financiero(mf);
	
		//Estimaion preventiva para riesgos
		Double eprcDeOficina = (sucursal != 58) ? pr.getEPRC(mes, anio, sucursal) : pr.getEprcConsolidado(mes, anio);		
		eprcDeOficina = (eprcDeOficina == null) ? 0 : eprcDeOficina;
		eprcDeOficina = (sucursal != 58) ? (eprcDeOficina <= 0) ? 0 :Math.abs(eprcDeOficina) : eprcDeOficina;
		miCaratula.setEpcr(eprcDeOficina);
		
		//Margen financiero ajustado 
		miCaratula.setMargen_financiero_ajustado((miCaratula.getMargen_financiero() - eprcDeOficina));
		
		//Comiciones cobradas
		Double cc = this.getBalnzaCaratula(sucursal, anio, mes, "430");
		cc = (cc == null)? 0 : cc*-1;
		miCaratula.setComiciones_cobradas(cc);
		
		//Comiciones pagadas
		Double cp = this.getBalnzaCaratula(sucursal, anio, mes, "530");
		cp = (cp == null)? 0 : cp;
		miCaratula.setComiciones_pagadas(cp);
		
		//Resultado por intermediacion
		Double rpi = this.getBalnzaCaratula(sucursal, anio, mes, "440");
		rpi = (rpi == null) ? 0: rpi;
		miCaratula.setResultado_intermediacion(rpi);
		
		//Otros ingresos
		Double oio = this.getBalnzaCaratula(sucursal, anio, mes, "450");
		oio = (oio == null)? 0 : oio * -1;
		Double aux = this.getBalnzaCaratula(sucursal, anio, mes, "450070000000001");
		oio += (aux == null)? 0 : aux;
		Double eprcConsolidado2 = (pr.getEprcConsolidado2(mes, anio) == null)? 0 : pr.getEprcConsolidado2(mes, anio);
		eprcDeOficina = (pr.getEPRC(mes, anio, sucursal) == null)? 0 : pr.getEPRC(mes, anio, sucursal);
		oio += (sucursal == 58) ? eprcConsolidado2 : (eprcDeOficina <= 0) ? Math.abs(eprcDeOficina):0;
		miCaratula.setOtros_ingresos(oio);
		
		//Gastos de  administracion y promocion
		Double gap = this.getBalnzaCaratula(sucursal, anio, mes, "540");
		gap  = (gap != null) ? gap : 0 ;
		gap += (sucursal != 57) ? (gastoCas/totalPersonal) * personalOficina : 0;
		gap = (Double.isNaN(gap)) ? 0: gap;
		miCaratula.setGap(gap);
		
		//Resultado antes de impuesto a la utilidad
		Double raiu = miCaratula.getMargen_financiero_ajustado() +
					  miCaratula.getComiciones_cobradas() -
					  miCaratula.getComiciones_pagadas() + 
					  miCaratula.getResultado_intermediacion() +
					  miCaratula.getOtros_ingresos() -
					  miCaratula.getGap();
		miCaratula.setResultado_antes_impuesto(raiu);
		
		//Impuesto a la utilidad causado
		Double iuc = this.getBalnzaCaratula(sucursal, anio, mes, "550000000");
		iuc  = (iuc != null) 	? iuc : 0 ;
		miCaratula.setImpuestos_causados(iuc);
		
		//Resultado antes de las operaciones discontinuadas
		miCaratula.setResultado_antes_operaciones(raiu-iuc);
		
		//Operaciones Discontinuadas
		Double opd = this.getBalnzaCaratula(sucursal, anio, mes, "470");
		opd = (opd==null) ? 0 : opd;
		miCaratula.setOperaciones_discontinuadas(opd);
		
		//Resultado neto
		miCaratula.setResultado_neto((raiu - iuc) - opd);
		
		//Indice de auto suficiencia
		Double idcauto = 	(miCaratula.getIngresos_intereses() +
							miCaratula.getComiciones_cobradas() +
							miCaratula.getOtros_ingresos())/
							(miCaratula.getGastos_intereses()+
							miCaratula.getEpcr()+
							miCaratula.getComiciones_pagadas()+
							miCaratula.getGap()+
							miCaratula.getImpuestos_causados());
		miCaratula.setIndice_autosuficiencia_operativa(idcauto);
		
		return miCaratula;
	}

	@GetMapping("/get/caratula/lista/{sucursal}/{anio}")
	public List<Double> getCaratulaLista(@PathVariable("sucursal") int sucursal, @PathVariable("anio") int anio){
		List<Double> resultado_neto = new ArrayList<Double>();
		Double acum = 0.0;
		for(int i = 1; i <= 12; i++) {
			Caratula temp = this.getCaratula(sucursal, anio, i);
			
			if(Double.isNaN(temp.getIndice_autosuficiencia_operativa())) {
				resultado_neto.add(0.0);
			}else {
				Double res = (double)Math.round(temp.getResultado_neto() * 100d) / 100d;
				resultado_neto.add(res);
				acum += res;
			}
				
		}
		resultado_neto.add(acum);
		return resultado_neto;
	}

	@DeleteMapping("/delete/balanza/{anio}/{mes}/{sucursal}")
	public boolean deleteBalanza(@PathVariable("anio") int anio, @PathVariable("mes") int mes, @PathVariable("sucursal") int sucursal ){
		this.BalanzaS.deleteBalanza(anio, mes, sucursal);
		return true;
	}
	
	@GetMapping("/get/caratula/{anio}/{mes}/{region}")
	public HashMap<String,Caratula> getCaratulaRegion(@PathVariable("anio") int anio,
			@PathVariable("mes") int mes, @PathVariable("region") int region){
		
		List<Sucursal> sucursales = s.getSucursalesRegion(region);
		result = new HashMap<>();
		Caratula cons = new Caratula();
		for(Sucursal sc : sucursales){
			Caratula aux = this.getCaratula((int)sc.getId(), anio, mes);
			cons.setComiciones_cobradas(cons.getComiciones_cobradas() + aux.getComiciones_cobradas());
			cons.setComiciones_pagadas(cons.getComiciones_pagadas() + aux.getComiciones_pagadas());
			cons.setEpcr(cons.getEpcr() + aux.getEpcr());
			cons.setGap(cons.getGap() + aux.getGap());
			cons.setGastos_intereses(cons.getGastos_intereses() + aux.getGastos_intereses());
			cons.setImpuestos_causados(cons.getImpuestos_causados() + aux.getImpuestos_causados());
			cons.setIndice_autosuficiencia_operativa(cons.getIndice_autosuficiencia_operativa() + aux.getIndice_autosuficiencia_operativa());
			cons.setIngresos_derivados_inversiones(cons.getIngresos_derivados_inversiones() +  aux.getIngresos_derivados_inversiones());
			cons.setIngresos_intereses(cons.getIngresos_intereses() + aux.getIngresos_intereses());
			cons.setMargen_financiero(cons.getMargen_financiero() + aux.getMargen_financiero());
			cons.setMargen_financiero_ajustado(cons.getMargen_financiero_ajustado() + aux.getMargen_financiero_ajustado());
			cons.setOperaciones_discontinuadas(cons.getOperaciones_discontinuadas() + aux.getOperaciones_discontinuadas());
			cons.setOtros_ingresos(cons.getOtros_ingresos() + aux.getOtros_ingresos());
			cons.setResultado_antes_impuesto(cons.getResultado_antes_impuesto() + aux.getResultado_antes_impuesto());
			cons.setResultado_antes_operaciones(cons.getResultado_antes_operaciones() + aux.getResultado_antes_operaciones());
			cons.setResultado_intermediacion(cons.getResultado_intermediacion() + aux.getResultado_intermediacion());
			cons.setResultado_neto(cons.getResultado_neto() + aux.getResultado_neto());
			cons.setResultado_pocicion_monetaria_neto(cons.getResultado_pocicion_monetaria_neto() + aux.getResultado_pocicion_monetaria_neto());
			result.put(sc.getNombre(),aux);
		}
		//Indice de auto suficiencia
		Double idcauto = (cons.getIngresos_intereses() +
							cons.getComiciones_cobradas() +
							cons.getOtros_ingresos())/
							(cons.getGastos_intereses()+
								cons.getEpcr()+
								cons.getComiciones_pagadas()+
								cons.getGap()+
								cons.getImpuestos_causados());
				cons.setIndice_autosuficiencia_operativa(idcauto);
		
		try{
			if(Double.isNaN(idcauto)){
				result = null;
				throw new RuntimeException("El estado de resultados, no se puede calcular");
			}else{
				result.put("CONS", cons);
			}
		}catch(RuntimeException e){
			System.out.println(e.getMessage());
		}
		
		return result;
	}  
	
	@GetMapping("/get/dashboard/ingresos/{anio}/{sucursal}")
	public List <Double> getIngresosPorInteresesResult (@PathVariable("sucursal")int sucursal, @PathVariable("anio")int anio) {
		List<Double> ingreso_interes = new ArrayList<Double>();
		Double acum = 0.0;
		for(int i = 1; i <= 12; i++) {
			Caratula temp = this.getCaratula(sucursal, anio, i);
			if(Double.isNaN(temp.getIndice_autosuficiencia_operativa())) {
				ingreso_interes.add(0.0);
			}else {
				Double res = temp.getIngresos_intereses()/1000000.00;
				ingreso_interes.add(res);
				acum += res;
			}
				
		}
		ingreso_interes.add(acum);
		return ingreso_interes;
	}
	
	@GetMapping("/get/dashboard/gastosfin/{anio}/{sucursal}")
	public List <Double> getGastosFinResult (@PathVariable("sucursal")int sucursal, @PathVariable("anio")int anio) {
		List<Double> gasto_interes = new ArrayList<Double>();
		Double acum = 0.0;
		for(int i = 1; i <= 12; i++) {
			Caratula temp = this.getCaratula(sucursal, anio, i);
			if(Double.isNaN(temp.getIndice_autosuficiencia_operativa())) {
				gasto_interes.add(0.0);
			}else {
				Double res = temp.getGastos_intereses()/1000000.00;
				gasto_interes.add(res);
				acum += res;
			}
				
		}
		gasto_interes.add(acum);
		return gasto_interes;
	}

	@GetMapping("/get/dashboard/eprc/{anio}/{sucursal}")
	public List <Double> getGastosEPRCResult (@PathVariable("sucursal")int sucursal, @PathVariable("anio")int anio) {
		List<Double> gasto_eprc = new ArrayList<Double>();
		Double acum = 0.0;
		for(int i = 1; i <= 12; i++) {
			Caratula temp = this.getCaratula(sucursal, anio, i);
			if(Double.isNaN(temp.getIndice_autosuficiencia_operativa())) {
				gasto_eprc.add(0.0);
			}else {
				Double res = temp.getEpcr()/1000000.00;
				gasto_eprc.add(res);
				acum += res;
			}
				
		}
		gasto_eprc.add(acum);
		return gasto_eprc;
	}
	
	@GetMapping("/get/dashboard/gap/{anio}/{sucursal}")
	public List <Double> getGastosGAPResult (@PathVariable("sucursal")int sucursal, @PathVariable("anio")int anio) {
		List<Double> gasto_gap = new ArrayList<Double>();
		Double acum = 0.0;
		for(int i = 1; i <= 12; i++) {
			Caratula temp = this.getCaratula(sucursal, anio, i);
			if(Double.isNaN(temp.getIndice_autosuficiencia_operativa())) {
				gasto_gap.add(0.0);
			}else {
				Double res = temp.getGap()/1000000.00;
				gasto_gap.add(res);
				acum += res;
			}
				
		}
		gasto_gap.add(acum);
		return gasto_gap;
	}
	
	@GetMapping("/get/dashboard/ingresos/{anio}/region/{region}")
	public List <List<Double>> getIngresosPorInteresesResultReg (@PathVariable("region")int region, @PathVariable("anio")int anio) {
		List<Double> ingreso_interes = new ArrayList<Double>();
		List<Double> gastos_interes  = new ArrayList<Double>();
		List<Double> eprc  = new ArrayList<Double>();
		List<Double> gap  = new ArrayList<Double>();
		
		Double acum_i = 0.0;
		Double acum_g = 0.0;
		Double acum_e = 0.0;
		Double acum_o = 0.0;
		
		for(int i = 1; i <= 12; i++) {
			HashMap<String, Caratula> templ = this.getCaratulaRegion(anio, i, region);
			if(templ != null){
				Caratula tempc = this.getCaratulaRegion(anio, i, region).get("CONS");
				if(Double.isNaN(tempc.getIndice_autosuficiencia_operativa())) {
					ingreso_interes.add(0.0);
					gastos_interes.add(0.0);
					eprc.add(0.0);
					gap.add(0.0);
				}else {
					Double res = tempc.getIngresos_intereses()/1000000.00;
					ingreso_interes.add(res);
					acum_i += res;
					
					res = tempc.getGastos_intereses()/1000000.00;
					gastos_interes.add(res);
					acum_g += res;
					
					res = tempc.getEpcr()/1000000.00;
					eprc.add(res);
					acum_e += res;
					
					res = tempc.getGap()/1000000.00;
					gap.add(res);
					acum_o += res;
				}
			}else{
				ingreso_interes.add(0.0);
				gastos_interes.add(0.0);
				eprc.add(0.0);
				gap.add(0.0);
			}
				
		}
		ingreso_interes.add(acum_i);
		gastos_interes.add(acum_g);
		eprc.add(acum_e);
		gap.add(acum_o);
		
		List<List<Double>> datos = new ArrayList<>();
		datos.add(ingreso_interes);
		datos.add(gastos_interes);
		datos.add(eprc);
		datos.add(gap);
		
		return datos;
	}
	
	public List<ObjectReport> getCaratulaReport(int sucursal, int anio, int mes){
		
		List<ObjectReport> lista = new ArrayList<>();
		Caratula aux = this.getCaratula(sucursal, anio -1, 12);
		lista.add(new ObjectReport("Ingresos por Interes",aux.getMes(),aux.getIngresos_intereses() ));
		lista.add(new ObjectReport("Gastos por intereses",aux.getMes(),aux.getGastos_intereses()));
		lista.add(new ObjectReport("Resultado por pocición monetaria neto (Margen financiero)",aux.getMes(),aux.getResultado_pocicion_monetaria_neto() ));
		lista.add(new ObjectReport("  MARGEN FINANCIERO",aux.getMes(),aux.getMargen_financiero()));
		lista.add(new ObjectReport("Estimación preventiva por riesgos créditicios",aux.getMes(),aux.getEpcr()));
		lista.add(new ObjectReport("  MARGEN FINANCIERO AJSUTADO POR RIESGOS CREDITICIOS",aux.getMes(),aux.getGastos_intereses()));
		lista.add(new ObjectReport("Comiciones y tarifas cobradas",aux.getMes(),aux.getComiciones_cobradas()));
		lista.add(new ObjectReport("Comiciones y tarifas pagadas",aux.getMes(),aux.getComiciones_pagadas()));
		lista.add(new ObjectReport("Resultado por intermediacion",aux.getMes(),aux.getResultado_intermediacion()));
		lista.add(new ObjectReport("Otros ingresos (egresos) de la operación",aux.getMes(),aux.getOtros_ingresos()));
		lista.add(new ObjectReport("Gastos de asministración y promoción",aux.getMes(),aux.getGap()));
		lista.add(new ObjectReport("  RESULTADO DE LA OPERACION",aux.getMes(),aux.getResultado_antes_impuesto()));
		lista.add(new ObjectReport("Participacion en el resultado de subsidiarias no consolidadas y asociadas",aux.getMes(),0.0));
		lista.add(new ObjectReport("  RESULTADO ANTES DE OPERACIONES DISCONTINUADAS",aux.getMes(),aux.getResultado_antes_operaciones()));
		lista.add(new ObjectReport("Operaciones discontinuadas",aux.getMes(),aux.getOperaciones_discontinuadas()));
		lista.add(new ObjectReport("  RESULTADO NETO",aux.getMes(),aux.getResultado_neto()));
		for(int i =1; i<=mes;i++){
			aux = this.getCaratula(sucursal, anio, i);
			lista.add(new ObjectReport("Ingresos por Interes",aux.getMes(),aux.getIngresos_intereses() ));
			lista.add(new ObjectReport("Gastos por intereses",aux.getMes(),aux.getGastos_intereses()));
			lista.add(new ObjectReport("Resultado por pocición monetaria neto (Margen financiero)",aux.getMes(),aux.getResultado_pocicion_monetaria_neto() ));
			lista.add(new ObjectReport("  MARGEN FINANCIERO",aux.getMes(),aux.getMargen_financiero()));
			lista.add(new ObjectReport("Estimación preventiva por riesgos créditicios",aux.getMes(),aux.getEpcr()));
			lista.add(new ObjectReport("  MARGEN FINANCIERO AJSUTADO POR RIESGOS CREDITICIOS",aux.getMes(),aux.getGastos_intereses()));
			lista.add(new ObjectReport("Comiciones y tarifas cobradas",aux.getMes(),aux.getComiciones_cobradas()));
			lista.add(new ObjectReport("Comiciones y tarifas pagadas",aux.getMes(),aux.getComiciones_pagadas()));
			lista.add(new ObjectReport("Resultado por intermediacion",aux.getMes(),aux.getResultado_intermediacion()));
			lista.add(new ObjectReport("Otros ingresos (egresos) de la operación",aux.getMes(),aux.getOtros_ingresos()));
			lista.add(new ObjectReport("Gastos de asministración y promoción",aux.getMes(),aux.getGap()));
			lista.add(new ObjectReport("  RESULTADO DE LA OPERACION",aux.getMes(),aux.getResultado_antes_impuesto()));
			lista.add(new ObjectReport("Participacion en el resultado de subsidiarias no consolidadas y asociadas",aux.getMes(),0.0));
			lista.add(new ObjectReport("  RESULTADO ANTES DE OPERACIONES DISCONTINUADAS",aux.getMes(),aux.getResultado_antes_operaciones()));
			lista.add(new ObjectReport("Operaciones discontinuadas",aux.getMes(),aux.getOperaciones_discontinuadas()));
			lista.add(new ObjectReport("  RESULTADO NETO",aux.getMes(),aux.getResultado_neto()));
		}
		return lista;
	}
	
	public List<ObjectReport> getListaIndicadores (int sucursal, int anio, int mes){
		List<ObjectReport> lista = new ArrayList<>();
		List<Mes> meseses = meses.getMeses();

		for(int i =1 ; i<= mes; i++){
			int aux_mes = i;
			Integer personal = 0;
			if(sucursal != 58){
				personal = pr.getPersonal(aux_mes, anio, sucursal);
			}
			if(personal == null){
				personal = 0;
			}
			Double cartera = pr.getCartera(aux_mes, anio, sucursal);
			String mesnombre = meseses.stream()
			.filter(mest -> mest.getId_mes() == aux_mes).findFirst().get().getNombre();
			lista.add(new ObjectReport("Numero de Clientes",mesnombre,0.0));
			lista.add(new ObjectReport("Saldo de Cartera",mesnombre,cartera));
			lista.add(new ObjectReport("Normalidad",mesnombre,0.0));
			lista.add(new ObjectReport("% Cartera Vencida",mesnombre,0.0));
			lista.add(new ObjectReport("Credito Promedio",mesnombre,0.0));
			lista.add(new ObjectReport("Clientes Promedio Por Asesor",mesnombre,0.0));
			lista.add(new ObjectReport("Cartera Promedio Por Aseor",mesnombre,0.0));
			lista.add(new ObjectReport("Plantilla Total",mesnombre, personal.doubleValue()));
			lista.add(new ObjectReport("Analista de Credito",mesnombre,0.0));
			lista.add(new ObjectReport("Indice de Aut. Operativa del Mes",mesnombre,0.0));
		}
		return lista;
	}
	
	public List<ObjectReport> getEresultReport(int sucursal, int anio, int mes){
		
		List<ObjectReport> lista = new ArrayList<>();
		Caratula aux = this.getCaratula(sucursal, anio -1, 12);
		lista.add(new ObjectReport("Resultado Neto",aux.getMes(),aux.getResultado_neto()));
		for(int i =1; i<=mes;i++){
			aux = this.getCaratula(sucursal, anio, i);
			lista.add(new ObjectReport("Resultado Neto",aux.getMes(),aux.getResultado_neto()));
		}
		return lista;
	}
	

}
































