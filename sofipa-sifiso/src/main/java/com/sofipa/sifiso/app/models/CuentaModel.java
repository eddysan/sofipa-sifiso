package com.sofipa.sifiso.app.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tabla_cuentas")
public class CuentaModel implements Serializable{
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_cuenta")
    private long id;
    private String clave_cuenta;
    private String nombre_cuenta;

    
    public long getId() {
        return id;
    }

    public String getClave_cuenta() {
        return clave_cuenta;
    }

    public String getNombre_cuenta() {
        return nombre_cuenta;
    }

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setClave_cuenta(String clave_cuenta) {
		this.clave_cuenta = clave_cuenta;
	}

	public void setNombre_cuenta(String nombre_cuenta) {
		this.nombre_cuenta = nombre_cuenta;
	}
    
    
    
}
