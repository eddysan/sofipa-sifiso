package com.sofipa.sifiso.app.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "edo_resultados_sucursal")
public class EstadoDeResultados {

	static final long serialversionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_registro")
	int id;
	int anio;
	int mes;
	int sucursal;
	Double ingresos_intereses;
	Double gastos_intereses;
	Double resultado_monterio;
	Double margen_financiero;
	Double estimacion_preventiva;
	Double margen_ajustado;
	Double comisiones_cobradas;
	Double comisiones_pagadas;
	Double resultado_intermediacion;
	Double otros_ingresos;
	Double gap;
	Double prorrateo_cas;
	Double resultado_operacion;
	Double participacion;
	Double resultado_antes_oper;
	Double operaciones_dis;
	Double resultado_neto;
	
	
	
	

	public EstadoDeResultados() {
		super();
		// TODO Auto-generated constructor stub
	}





	public EstadoDeResultados(int id, int anio, int mes, int sucursal, Double ingresos_intereses,
			Double gastos_intereses, Double resultado_monterio, Double margen_financiero, Double estimacion_preventiva,
			Double margen_ajustado, Double comisiones_cobradas, Double comisiones_pagadas,
			Double resultado_intermediacion, Double otros_ingresos, Double gap, Double prorrateo_cas,
			Double resultado_operacion, Double participacion, Double resultado_antes_oper, Double operaciones_dis,
			Double resultado_neto) {
		super();
		this.id = id;
		this.anio = anio;
		this.mes = mes;
		this.sucursal = sucursal;
		this.ingresos_intereses = ingresos_intereses;
		this.gastos_intereses = gastos_intereses;
		this.resultado_monterio = resultado_monterio;
		this.margen_financiero = margen_financiero;
		this.estimacion_preventiva = estimacion_preventiva;
		this.margen_ajustado = margen_ajustado;
		this.comisiones_cobradas = comisiones_cobradas;
		this.comisiones_pagadas = comisiones_pagadas;
		this.resultado_intermediacion = resultado_intermediacion;
		this.otros_ingresos = otros_ingresos;
		this.gap = gap;
		this.prorrateo_cas = prorrateo_cas;
		this.resultado_operacion = resultado_operacion;
		this.participacion = participacion;
		this.resultado_antes_oper = resultado_antes_oper;
		this.operaciones_dis = operaciones_dis;
		this.resultado_neto = resultado_neto;
	}





	public static long getSerialversionuid() {
		return serialversionUID;
	}





	public int getId() {
		return id;
	}





	public void setId(int id) {
		this.id = id;
	}





	public int getAnio() {
		return anio;
	}





	public void setAnio(int anio) {
		this.anio = anio;
	}





	public int getMes() {
		return mes;
	}





	public void setMes(int mes) {
		this.mes = mes;
	}





	public int getSucursal() {
		return sucursal;
	}





	public void setSucursal(int sucursal) {
		this.sucursal = sucursal;
	}





	public Double getIngresos_intereses() {
		return ingresos_intereses;
	}





	public void setIngresos_intereses(Double ingresos_intereses) {
		this.ingresos_intereses = ingresos_intereses;
	}





	public Double getGastos_intereses() {
		return gastos_intereses;
	}





	public void setGastos_intereses(Double gastos_intereses) {
		this.gastos_intereses = gastos_intereses;
	}





	public Double getResultado_monterio() {
		return resultado_monterio;
	}





	public void setResultado_monterio(Double resultado_monterio) {
		this.resultado_monterio = resultado_monterio;
	}





	public Double getMargen_financiero() {
		return margen_financiero;
	}





	public void setMargen_financiero(Double margen_financiero) {
		this.margen_financiero = margen_financiero;
	}





	public Double getEstimacion_preventiva() {
		return estimacion_preventiva;
	}





	public void setEstimacion_preventiva(Double estimacion_preventiva) {
		this.estimacion_preventiva = estimacion_preventiva;
	}





	public Double getMargen_ajustado() {
		return margen_ajustado;
	}





	public void setMargen_ajustado(Double margen_ajustado) {
		this.margen_ajustado = margen_ajustado;
	}





	public Double getComisiones_cobradas() {
		return comisiones_cobradas;
	}





	public void setComisiones_cobradas(Double comisiones_cobradas) {
		this.comisiones_cobradas = comisiones_cobradas;
	}





	public Double getComisiones_pagadas() {
		return comisiones_pagadas;
	}





	public void setComisiones_pagadas(Double comisiones_pagadas) {
		this.comisiones_pagadas = comisiones_pagadas;
	}





	public Double getResultado_intermediacion() {
		return resultado_intermediacion;
	}





	public void setResultado_intermediacion(Double resultado_intermediacion) {
		this.resultado_intermediacion = resultado_intermediacion;
	}





	public Double getOtros_ingresos() {
		return otros_ingresos;
	}





	public void setOtros_ingresos(Double otros_ingresos) {
		this.otros_ingresos = otros_ingresos;
	}





	public Double getGap() {
		return gap;
	}





	public void setGap(Double gap) {
		this.gap = gap;
	}





	public Double getProrrateo_cas() {
		return prorrateo_cas;
	}





	public void setProrrateo_cas(Double prorrateo_cas) {
		this.prorrateo_cas = prorrateo_cas;
	}





	public Double getResultado_operacion() {
		return resultado_operacion;
	}





	public void setResultado_operacion(Double resultado_operacion) {
		this.resultado_operacion = resultado_operacion;
	}





	public Double getParticipacion() {
		return participacion;
	}





	public void setParticipacion(Double participacion) {
		this.participacion = participacion;
	}





	public Double getResultado_antes_oper() {
		return resultado_antes_oper;
	}





	public void setResultado_antes_oper(Double resultado_antes_oper) {
		this.resultado_antes_oper = resultado_antes_oper;
	}





	public Double getOperaciones_dis() {
		return operaciones_dis;
	}





	public void setOperaciones_dis(Double operaciones_dis) {
		this.operaciones_dis = operaciones_dis;
	}





	public Double getResultado_neto() {
		return resultado_neto;
	}





	public void setResultado_neto(Double resultado_neto) {
		this.resultado_neto = resultado_neto;
	}	
}
