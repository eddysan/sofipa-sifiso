package com.sofipa.sifiso.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import com.sofipa.sifiso.app.models.Region;


public interface IRegionDAO extends Repository<Region, Long>{
	
	@Query("From Region")
	public List<Region>getRegiones();
	
}
