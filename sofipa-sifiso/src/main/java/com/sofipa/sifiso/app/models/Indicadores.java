package com.sofipa.sifiso.app.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="indicadores_sucursal")
public class Indicadores {
	private static final long serialversionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_registro")
	private int id_registro;
	@ManyToOne
	@JoinColumn(name="sucursal", nullable = false) 
	private Sucursal sucursal;
	
	private int numero_clientes;
	private Double saldo_cartera;
	private Double normalidad;
	private Double carteravencida_percent;
	private Double credito_promedio;
	private Double clientes_promedio;
	private Double cartera_promedio;
	private Double plantilla_total;
	private Double analista_credito;
	private Double indice_auto_suf_mes;
	private Double indice_auto_suf_acum;
	private int mes;
	private int anio;
	
	
	
	public Indicadores() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public int getId_registro() {
		return id_registro;
	}
	public void setId_registro(int id_registro) {
		this.id_registro = id_registro;
	}
	public Sucursal getSucursal() {
		return sucursal;
	}
	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}
	public int getNumero_clientes() {
		return numero_clientes;
	}
	public void setNumero_clientes(int numero_clientes) {
		this.numero_clientes = numero_clientes;
	}
	public Double getSaldo_cartera() {
		return saldo_cartera;
	}
	public void setSaldo_cartera(Double saldo_cartera) {
		this.saldo_cartera = saldo_cartera;
	}
	public Double getNormalidad() {
		return normalidad;
	}
	public void setNormalidad(Double normalidad) {
		this.normalidad = normalidad;
	}
	public Double getCarteravencida_percent() {
		return carteravencida_percent;
	}
	public void setCarteravencida_percent(Double carteravencida_percent) {
		this.carteravencida_percent = carteravencida_percent;
	}
	public Double getCredito_promedio() {
		return credito_promedio;
	}
	public void setCredito_promedio(Double credito_promedio) {
		this.credito_promedio = credito_promedio;
	}
	public Double getClientes_promedio() {
		return clientes_promedio;
	}
	public void setClientes_promedio(Double clientes_promedio) {
		this.clientes_promedio = clientes_promedio;
	}
	public Double getCartera_promedio() {
		return cartera_promedio;
	}
	public void setCartera_promedio(Double cartera_promedio) {
		this.cartera_promedio = cartera_promedio;
	}
	public Double getPlantilla_total() {
		return plantilla_total;
	}
	public void setPlantilla_total(Double plantilla_total) {
		this.plantilla_total = plantilla_total;
	}
	public Double getAnalista_credito() {
		return analista_credito;
	}
	public void setAnalista_credito(Double analista_credito) {
		this.analista_credito = analista_credito;
	}
	public Double getIndice_auto_suf_mes() {
		return indice_auto_suf_mes;
	}
	public void setIndice_auto_suf_mes(Double indice_auto_suf_mes) {
		this.indice_auto_suf_mes = indice_auto_suf_mes;
	}
	public Double getIndice_auto_suf_acum() {
		return indice_auto_suf_acum;
	}
	public void setIndice_auto_suf_acum(Double indice_auto_suf_acum) {
		this.indice_auto_suf_acum = indice_auto_suf_acum;
	}
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public int getAnio() {
		return anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
	public static long getSerialversionuid() {
		return serialversionUID;
	}
	
}
