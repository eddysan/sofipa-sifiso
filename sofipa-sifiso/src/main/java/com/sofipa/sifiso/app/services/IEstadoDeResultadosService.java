package com.sofipa.sifiso.app.services;



import com.sofipa.sifiso.app.models.EstadoDeResultados;

public interface IEstadoDeResultadosService {
	
	public EstadoDeResultados getEstadoResultados(int anio, int mes, int sucursal);
}
