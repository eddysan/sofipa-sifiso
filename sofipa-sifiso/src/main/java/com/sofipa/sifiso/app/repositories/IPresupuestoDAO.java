package com.sofipa.sifiso.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.sofipa.sifiso.app.models.Presupuesto;

public interface IPresupuestoDAO extends Repository<Presupuesto, Long>{
	
	@Query("FROM Presupuesto p INNER JOIN MixCuentas mx ON "
			+ "p.cuenta.id_cuenta_mapeo = mx.cuenta_mapeo.id_cuenta_mapeo "
			+ "WHERE p.anio = :anio and p.mes = :mes and p.sucursal = :sucursal and mx.grupo.id_grupo = :grupo")
	List<Presupuesto> getPresupuesto(@Param("anio") int anio, @Param("mes") int mes,@Param("sucursal") int sucursal, @Param("grupo") long grupo);
	
	@Query("From Presupuesto p Where p.anio = :anio and p.mes = :mes and p.sucursal = :sucursal")
	List<Presupuesto> getPresupuesto(@Param("anio") int anio, @Param("mes") int mes,@Param("sucursal") int sucursal);
}