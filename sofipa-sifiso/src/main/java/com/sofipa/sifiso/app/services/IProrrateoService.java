package com.sofipa.sifiso.app.services;

import java.util.List;

import com.sofipa.sifiso.app.controllers.ProrrateoController.ProrrateoAux;
import com.sofipa.sifiso.app.models.Prorrateo;


public interface IProrrateoService {
	boolean insertDatosProrrateo(int mes, int anio, ProrrateoAux[] datos);
	
	Double getTotalCartera(int mes, int anio);
	
	Integer getTotalPersonal(int mes, int anio);
	
	Integer getPersonal(int mes, int anio, int sucursal);
	
	Double getCartera(int mes, int anio, int sucursal);
	
	Double getEPRC(int mes, int anio, int sucursal);
	
	Double getEprcConsolidado (int mes, int anio);
	
	Double getEprcConsolidado2 (int mes, int anio);
	
	List<Prorrateo> getProrrateo(int mes, int anio);
	
	void deleteProrrateo (int anio, int mes);
}
