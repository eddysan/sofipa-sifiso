package com.sofipa.sifiso.app.models;

public class Caratula {
	
	private String mes; 
	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}
	private Double indice_autosuficiencia_operativa;
	private Double ingresos_intereses;
	private Double ingresos_derivados_inversiones; 
	private Double gastos_intereses;
	private Double resultado_pocicion_monetaria_neto;
		private Double margen_financiero;
	
	private Double epcr;
		private Double margen_financiero_ajustado;
	
	private Double comiciones_cobradas;
	private Double comiciones_pagadas;
	private Double resultado_intermediacion;
	private Double otros_ingresos;
	private Double gap;
		private Double resultado_antes_impuesto;
	
	private Double impuestos_causados;
		private Double resultado_antes_operaciones;
	
	private Double operaciones_discontinuadas;
		private Double resultado_neto;
	
	public Caratula() {
		super();
		this.indice_autosuficiencia_operativa  = 
				this.ingresos_intereses = 
				this.gastos_intereses = 
				this.resultado_pocicion_monetaria_neto = 
				this.margen_financiero = 
				this.epcr = 
				this.margen_financiero_ajustado = 
				this.comiciones_cobradas = 
				this.comiciones_pagadas = 
				this.resultado_intermediacion = 
				this.otros_ingresos = 
				this.gap = 
				this.resultado_antes_impuesto = 
				this.impuestos_causados = 
				this.resultado_antes_operaciones =
				this.operaciones_discontinuadas =
				this.resultado_neto = 
				this.ingresos_derivados_inversiones = 0.0;
		this.mes = "";
	}
	
	public Caratula(Double indice_autosuficiencia_operativa, Double ingresos_intereses, Double gastos_intereses,
			Double resultado_pocicion_monetaria_neto, Double margen_financiero, Double epcr,
			Double margen_financiero_ajustado, Double comiciones_cobradas, Double comiciones_pagadas,
			Double resultado_intermediacion, Double otros_ingresos, Double gap, Double resultado_antes_impuesto,
			Double impuestos_causados, Double resultado_antes_operaciones, Double operaciones_discontinuadas,
			Double resultado_neto, Double ingresos_derivados_inversiones, String mes) {
		super();
		this.indice_autosuficiencia_operativa = indice_autosuficiencia_operativa;
		this.ingresos_intereses = ingresos_intereses;
		this.gastos_intereses = gastos_intereses;
		this.resultado_pocicion_monetaria_neto = resultado_pocicion_monetaria_neto;
		this.margen_financiero = margen_financiero;
		this.epcr = epcr;
		this.margen_financiero_ajustado = margen_financiero_ajustado;
		this.comiciones_cobradas = comiciones_cobradas;
		this.comiciones_pagadas = comiciones_pagadas;
		this.resultado_intermediacion = resultado_intermediacion;
		this.otros_ingresos = otros_ingresos;
		this.gap = gap;
		this.resultado_antes_impuesto = resultado_antes_impuesto;
		this.impuestos_causados = impuestos_causados;
		this.resultado_antes_operaciones = resultado_antes_operaciones;
		this.operaciones_discontinuadas = operaciones_discontinuadas;
		this.resultado_neto = resultado_neto;
		this.ingresos_derivados_inversiones = ingresos_derivados_inversiones;
		this.mes = mes;
	}
	
	
	public Double getIngresos_derivados_inversiones() {
		return ingresos_derivados_inversiones;
	}

	public void setIngresos_derivados_inversiones(Double ingresos_derivados_inversiones) {
		this.ingresos_derivados_inversiones = ingresos_derivados_inversiones;
	}
	public Double getIndice_autosuficiencia_operativa() {
		return indice_autosuficiencia_operativa;
	}
	public void setIndice_autosuficiencia_operativa(Double indice_autosuficiencia_operativa) {
		this.indice_autosuficiencia_operativa = indice_autosuficiencia_operativa;
	}
	public Double getIngresos_intereses() {
		return ingresos_intereses;
	}
	public void setIngresos_intereses(Double ingresos_intereses) {
		this.ingresos_intereses = ingresos_intereses;
	}
	public Double getGastos_intereses() {
		return gastos_intereses;
	}
	public void setGastos_intereses(Double gastos_intereses) {
		this.gastos_intereses = gastos_intereses;
	}
	public Double getResultado_pocicion_monetaria_neto() {
		return resultado_pocicion_monetaria_neto;
	}
	public void setResultado_pocicion_monetaria_neto(Double resultado_pocicion_monetaria_neto) {
		this.resultado_pocicion_monetaria_neto = resultado_pocicion_monetaria_neto;
	}
	public Double getMargen_financiero() {
		return margen_financiero;
	}
	public void setMargen_financiero(Double margen_financiero) {
		this.margen_financiero = margen_financiero;
	}
	public Double getEpcr() {
		return epcr;
	}
	public void setEpcr(Double epcr) {
		this.epcr = epcr;
	}
	public Double getMargen_financiero_ajustado() {
		return margen_financiero_ajustado;
	}
	public void setMargen_financiero_ajustado(Double margen_financiero_ajustado) {
		this.margen_financiero_ajustado = margen_financiero_ajustado;
	}
	public Double getComiciones_cobradas() {
		return comiciones_cobradas;
	}
	public void setComiciones_cobradas(Double comiciones_cobradas) {
		this.comiciones_cobradas = comiciones_cobradas;
	}
	public Double getComiciones_pagadas() {
		return comiciones_pagadas;
	}
	public void setComiciones_pagadas(Double comiciones_pagadas) {
		this.comiciones_pagadas = comiciones_pagadas;
	}
	public Double getResultado_intermediacion() {
		return resultado_intermediacion;
	}
	public void setResultado_intermediacion(Double resultado_intermediacion) {
		this.resultado_intermediacion = resultado_intermediacion;
	}
	public Double getOtros_ingresos() {
		return otros_ingresos;
	}
	public void setOtros_ingresos(Double otros_ingresos) {
		this.otros_ingresos = otros_ingresos;
	}
	public Double getGap() {
		return gap;
	}
	public void setGap(Double gap) {
		this.gap = gap;
	}
	public Double getResultado_antes_impuesto() {
		return resultado_antes_impuesto;
	}
	public void setResultado_antes_impuesto(Double resultado_antes_impuesto) {
		this.resultado_antes_impuesto = resultado_antes_impuesto;
	}
	public Double getImpuestos_causados() {
		return impuestos_causados;
	}
	public void setImpuestos_causados(Double impuestos_causados) {
		this.impuestos_causados = impuestos_causados;
	}
	public Double getResultado_antes_operaciones() {
		return resultado_antes_operaciones;
	}
	public void setResultado_antes_operaciones(Double resultado_antes_operaciones) {
		this.resultado_antes_operaciones = resultado_antes_operaciones;
	}
	public Double getOperaciones_discontinuadas() {
		return operaciones_discontinuadas;
	}
	public void setOperaciones_discontinuadas(Double operaciones_discontinuadas) {
		this.operaciones_discontinuadas = operaciones_discontinuadas;
	}
	public Double getResultado_neto() {
		return resultado_neto;
	}
	public void setResultado_neto(Double resultado_neto) {
		this.resultado_neto = resultado_neto;
	}
	
	
}
