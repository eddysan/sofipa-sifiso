package com.sofipa.sifiso.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sofipa.sifiso.app.models.Indicadores;
import com.sofipa.sifiso.app.repositories.IIndicadoresDAO;

@Service
public class IndicadoresService implements IIndicadoresService{
	
	@Autowired
	private IIndicadoresDAO i;
	
	@Override
	public List<Indicadores> findAll() {
		// TODO Auto-generated method stub
		return i.findAll();
	}

	@Override
	public List<Indicadores> findByAnioAndMesLessThanAndSucursalId(int anio, int mes, Long sucursal) {
		// TODO Auto-generated method stub
		return i.findByAnioAndMesLessThanAndSucursalId(anio, mes, sucursal); 
	}
	
	@Override
	public Indicadores findByAnioAndMesAndSucursalId(int anio, int mes, Long sucursal) {
		// TODO Auto-generated method stub
		return i.findByAnioAndMesAndSucursalId(anio, mes, sucursal);
	}
	
	@Override
	public List<Indicadores> findByAnioAndMesLessThanAndSucursalRegionId(int anio, int mes, Long region) {
		// TODO Auto-generated method stub
		return i.findByAnioAndMesLessThanAndSucursalRegionId(anio, mes, region);
	}

	@Override
	public List<Indicadores> findByAnioAndMesAndSucursalRegionId(int anio, int mes, Long region) {
		// TODO Auto-generated method stub
		return i.findByAnioAndMesAndSucursalRegionId(anio, mes, region);
	}
	
	
}
