package com.sofipa.sifiso.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sofipa.sifiso.app.models.Anio;
import com.sofipa.sifiso.app.models.Mes;
import com.sofipa.sifiso.app.models.Sucursal;
import com.sofipa.sifiso.app.services.IAnioService;
import com.sofipa.sifiso.app.services.IMesService;
import com.sofipa.sifiso.app.services.ISucursalService;

@RestController
public class FechaController {
	
	@Autowired
	private IMesService mes;
	
	@Autowired
	private IAnioService anio;
	
	@Autowired
	private ISucursalService sucursal;
	
	@GetMapping("/get/meses")
	public List<Mes> getMeses(){
		return (List<Mes>) mes.getMeses();
	}
	
	@GetMapping("/get/anios")
	public List<Anio> getAnios(){
		return (List<Anio>) anio.getAnios();
	}
	
	@GetMapping("/get/sucursales")
	public List<Sucursal> getSucursales(){
		return (List<Sucursal>) sucursal.getSucursales();
	}
	
}
