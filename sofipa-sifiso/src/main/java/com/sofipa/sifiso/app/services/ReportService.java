package com.sofipa.sifiso.app.services;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sofipa.sifiso.app.models.ObjectReport;
import com.sofipa.sifiso.app.repositories.DaoReport;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;

@Service
public class ReportService {
	
	@Autowired(required = true)
	private DaoReport dr;
	
	public JasperPrint exportPDF(String sucursal, int anio, String mes, List<ObjectReport> c,List<ObjectReport> i,List<ObjectReport> g) throws JRException, IOException {
		  return dr.exportPDF(sucursal, anio, mes, c,i,g);
	 }
	
}
