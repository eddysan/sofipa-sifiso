package com.sofipa.sifiso.app.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sofipa.sifiso.app.models.Mes;

public interface IMesDAO extends CrudRepository<Mes, Long>{
	
}
