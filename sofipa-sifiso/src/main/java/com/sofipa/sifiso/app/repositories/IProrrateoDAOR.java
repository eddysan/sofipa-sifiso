package com.sofipa.sifiso.app.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sofipa.sifiso.app.models.Prorrateo;

public interface IProrrateoDAOR extends CrudRepository <Prorrateo, Long> {

}
