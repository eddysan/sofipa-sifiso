package com.sofipa.sifiso.app.controllers;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.sofipa.sifiso.app.models.CuentaModel;
import com.sofipa.sifiso.app.services.ICuentaService;

@RestController
public class CuentaController {
	
	 @Autowired
	    ICuentaService cuentaService;
	    
	    @GetMapping("/get/cuentas")
	    public List<CuentaModel> getCuentas(){
	        List<CuentaModel> cuentas = cuentaService.getCuentas(); 
	    	if (cuentas.isEmpty()) {
	    		return null;
	    	}else {
	    		return cuentas;
	    	}
	    }
	    
	    @GetMapping("/get/cuenta/{id_cuenta}")
	    public CuentaModel getCuenta(@PathVariable("id_cuenta") Long id_cuenta){
	    	CuentaModel cuenta = cuentaService.getCuenta(id_cuenta); 
	    	if (cuenta.equals(null)) {
	    		return null;
	    	}else {
	    		return cuenta;
	    	}
	    }
	    
}
