package com.sofipa.sifiso.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.sofipa.sifiso.app.models.Grupo;

public interface IGrupoDAO extends Repository<Grupo, Long>{
	
	@Query("SELECT g FROM Grupo g")
	public List<Grupo> getCuentasAgrupadas();
}
