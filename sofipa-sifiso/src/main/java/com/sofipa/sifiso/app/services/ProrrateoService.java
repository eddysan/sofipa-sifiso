package com.sofipa.sifiso.app.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sofipa.sifiso.app.controllers.ProrrateoController.ProrrateoAux;
import com.sofipa.sifiso.app.models.Prorrateo;
import com.sofipa.sifiso.app.repositories.IProrrateoDAO;
import com.sofipa.sifiso.app.repositories.IProrrateoDAOR;

@Service
public class ProrrateoService implements IProrrateoService {
	
	@Autowired
	private IProrrateoDAO prdr;
	
	@Autowired
	private IProrrateoDAOR pr;

	@Override
	@Transactional
	public boolean insertDatosProrrateo(int mes, int anio, ProrrateoAux[] datos) {
		for(ProrrateoAux element: datos) {
			Prorrateo nuevo = new Prorrateo();
			nuevo.setAnio(anio);
			nuevo.setMes(mes);
			nuevo.setPersonal(element.getPersonal());
			nuevo.setSucursal(element.getId());
			nuevo.setCartera(element.getCartera());
			nuevo.setEprc(element.getEprc());
			pr.save(nuevo);		
		}
		return true;
	}

	@Override
	public Double getTotalCartera(int mes, int anio) {
		// TODO Auto-generated method stub
		return prdr.getTotalCartera(mes, anio);
	}

	@Override
	public Integer getTotalPersonal(int mes, int anio) {
		// TODO Auto-generated method stub
		return prdr.getTotalPersonal(mes, anio);
	}

	@Override
	public Integer getPersonal(int mes, int anio, int sucursal) {
		// TODO Auto-generated method stub
		return prdr.getPersonal(mes, anio, sucursal);
	}

	@Override
	public Double getCartera(int mes, int anio, int sucursal) {
		// TODO Auto-generated method stub
		return prdr.getCartera(mes, anio, sucursal);
	}

	@Override
	public List<Prorrateo> getProrrateo(int mes, int anio) {
		return prdr.getProrrateo(mes, anio);
	}

	@Override
	public Double getEPRC(int mes, int anio, int sucursal) {
		// TODO Auto-generated method stub
		return prdr.getEstimacionPorRiesgosCrediticios(mes, anio, sucursal);
	}

	@Override
	public Double getEprcConsolidado(int mes, int anio) {
		return prdr.getEPRCConsolidado(mes, anio);
	}
	
	@Override
	public Double getEprcConsolidado2(int mes, int anio) {
		return prdr.getEPRCConsolidado2(mes, anio);
	}

	@Override
	public void deleteProrrateo(int anio, int mes) {
		prdr.deleteProrrateo(anio, mes);
	}
	
}
