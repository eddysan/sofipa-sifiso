package com.sofipa.sifiso.app.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.sofipa.sifiso.app.models.GrupoCuenta;

public interface IGrupoCuentaDAO extends Repository<GrupoCuenta, Long> {
	@Query("SELECT g From GrupoCuenta g Where g.nombre = :nombre")
	public GrupoCuenta getGrupo(@Param("nombre") String nombre);
	
}
