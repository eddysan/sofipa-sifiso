package com.sofipa.sifiso.app.services;


import java.util.List;

import com.sofipa.sifiso.app.models.Presupuesto;
import com.sofipa.sifiso.app.models.PresupuestoAux;


public interface IPresupuestoService {
	
	List<Presupuesto> getPresupuesto(int anio, int mes, int sucursal, long grupo);
	List<Presupuesto> getPresupuesto(int anio, int mes, int sucursal);
	void setPresupuesto(PresupuestoAux pres);
}
