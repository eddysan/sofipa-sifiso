package com.sofipa.sifiso.app.services;

import java.util.List;
import com.sofipa.sifiso.app.models.Sucursal;

public interface ISucursalService {
	public List<Sucursal> getSucursales();
	
	public List<Sucursal> getSucursalesRegion(long region);
}
