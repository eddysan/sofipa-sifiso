package com.sofipa.sifiso.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sofipa.sifiso.app.models.Anio;
import com.sofipa.sifiso.app.repositories.IAnioDAO;

@Service
public class AnioService implements IAnioService {

	@Autowired
	IAnioDAO anios;
	
	@Override
	public List<Anio> getAnios(){
		List<Anio> anios = (List<Anio>) this.anios.findAll();
		if(anios.isEmpty()) {
			return null;
		}else {
			return anios;
		}
	}
}
