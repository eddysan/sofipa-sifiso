package com.sofipa.sifiso.app.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sofipa.sifiso.app.models.EstadoDeResultados;
import com.sofipa.sifiso.app.repositories.EstadoDeResultadosDAO;

@Service
public class EstadoDeResultadosService implements IEstadoDeResultadosService{
	@Autowired
	EstadoDeResultadosDAO estado;

	@Override
	public EstadoDeResultados getEstadoResultados(int anio, int mes, int sucursal) {
		return estado.getEdoResult(anio, mes, sucursal);
	}

}
