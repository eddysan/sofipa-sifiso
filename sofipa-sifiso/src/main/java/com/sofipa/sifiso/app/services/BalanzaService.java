package com.sofipa.sifiso.app.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sofipa.sifiso.app.models.Balanza;
import com.sofipa.sifiso.app.models.CuentaModel;
import com.sofipa.sifiso.app.models.Movimiento;
import com.sofipa.sifiso.app.repositories.IBalanzaDAO;
import com.sofipa.sifiso.app.repositories.IBalanzaDAOR;
import com.sofipa.sifiso.app.repositories.ICuentaDAO;



@Service
public class BalanzaService implements IBalanzaService{
	
	@Autowired
    private IBalanzaDAO balanza;
	
	@Autowired
	private ICuentaDAO cuenta;
	
	@Autowired
	private IBalanzaDAOR balanzar;
	
	@Override
	public List<Balanza> getBalanza(int sucursal, int anio, int mes) {
		List<Balanza> balanzas = (List<Balanza>)this.balanza.findAll(); 
		List<Balanza> result = new ArrayList<>();
		for(Balanza bal: balanzas) {
			if(bal.getAnio() == anio && bal.getMes() == mes && bal.getSucursal() == sucursal) {
				result.add(bal);
			}
		}
		return result;
	}
	
	@Override
	@Transactional
	public boolean setNewBalanza(int anio, int mes, int sucursal, String movs){
		List<Balanza> bal_cm = (List<Balanza>) this.getBalanza(sucursal, anio, mes);
		if(bal_cm.isEmpty()) {
			Movimiento [] response = new Gson().fromJson(movs, Movimiento[].class);
			List<CuentaModel> cuentas =  (List<CuentaModel>) this.cuenta.findAll();	
			for(Movimiento object : response ) {
				long id_cuenta = 959;
				if(object.getClave_cuenta() != null ) {
					for(CuentaModel cuenta : cuentas) {
						if(object.getClave_cuenta().equals(cuenta.getClave_cuenta())) {
							id_cuenta = cuenta.getId(); 
						}
					}
					if(id_cuenta == 959 && object.getCargo() != null) {
						CuentaModel nueva_cuenta = new CuentaModel();
						nueva_cuenta.setNombre_cuenta(object.getCuenta().getNombre_cuenta());
						nueva_cuenta.setClave_cuenta(object.getClave_cuenta());
						id_cuenta = this.cuenta.save(nueva_cuenta).getId();
						System.out.println("CUENTA NO REGISTRADA: " + object.getCuenta().getNombre_cuenta());
					}
					System.out.println("id_cuenta: " + id_cuenta);
				} 
				
				//System.out.println(object.getSaldo_inicial()); 
				Balanza movimiento = new Balanza();
				movimiento.setSucursal(sucursal);
				movimiento.setAnio(anio);
				movimiento.setMes(mes);
				movimiento.setCuenta(cuenta.findById(id_cuenta).get());
				movimiento.setSaldo_inicial(object.getSaldo_inicial());
				movimiento.setCargo(object.getCargo());
				movimiento.setAbono(object.getAbono());
				movimiento.setSaldo_final(object.getSaldo_final());
				balanza.save(movimiento);
				if(id_cuenta == 959){
					break;
				}
			}
			
			return true;
		
		}
		return false;
	}

	@Override
	public Double getIngresosPorIntereses(int mes_act, int anio_act) {
		Optional <Balanza> consolidado =  this.balanzar.findMovIngresosPorIntereses(anio_act, mes_act);
		if(!consolidado.isPresent()) {
			return 0.0;
		}
		return -((consolidado.get().getSaldo_final()) - consolidado.get().getSaldo_inicial());
	}

	@Override
	public Double getGastosPorIntereses(int anio_act, int mes_act) {
		Optional <Balanza> consolidado =  this.balanzar.findMovGastosPorIntereses(anio_act, mes_act);
		if(!consolidado.isPresent()) {
			return 0.0;
		}
		return ((consolidado.get().getSaldo_final()) - consolidado.get().getSaldo_inicial());
	}

	@Override
	public List<Double> getIngresosAnio(int anio) {
		return  balanzar.getListIngresosAnio(anio);		
	}

	@Override
	public List<Double> getGastosAnio(int anio) {
		return balanzar.getListGastosAnio(anio);
	}

	@Override
	public Double getBalanzaPorCaratula(int sucursal, int anio, int mes, String cuenta) {
		return balanzar.getBalanzaParaCaratula(sucursal, anio, mes, cuenta);
	}

	@Override
	public Double getBalanzaPorCaratula2(int anio, int mes) {
		return balanzar.getBalanzaParaCaratula2(anio,mes);
	}

	@Override
	public List<Balanza> findDataProrrateo(int mes, int anio) {
		return balanzar.findDataProrrateo(mes, anio);
	}

	@Override
	public Double getTotalGastosInteres(int mes, int anio) {
		// TODO Auto-generated method stub
		return balanzar.getTotalGastosPorInteres(mes, anio);
	}

	@Override
	public void deleteBalanza(int anio, int mes, int sucursal) {
		this.balanzar.deleteBalanza(anio, mes, sucursal);
	}
	
}

