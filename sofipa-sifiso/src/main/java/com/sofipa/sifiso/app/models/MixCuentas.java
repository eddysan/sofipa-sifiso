package com.sofipa.sifiso.app.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tabla_grupo_cuenta")
public class MixCuentas implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_nexo")
	private long id_nexo;
	
	@ManyToOne
	@JoinColumn(name="grupo", nullable = false)
	private Grupo grupo;
	
	@ManyToOne
	@JoinColumn(name="cuenta_mapeo", nullable = false)
	private GrupoCuenta cuenta_mapeo;

	public MixCuentas(long id_nexo, Grupo grupo, GrupoCuenta cuenta_mapeo) {
		super();
		this.id_nexo = id_nexo;
		this.grupo = grupo;
		this.cuenta_mapeo = cuenta_mapeo;
	}

	public MixCuentas() {
		super();
	}

	public long getId_nexo() {
		return id_nexo;
	}

	public void setId_nexo(long id_nexo) {
		this.id_nexo = id_nexo;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	public GrupoCuenta getCuenta_mapeo() {
		return cuenta_mapeo;
	}

	public void setCuenta_mapeo(GrupoCuenta cuenta_mapeo) {
		this.cuenta_mapeo = cuenta_mapeo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
}
