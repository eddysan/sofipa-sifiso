package com.sofipa.sifiso.app.models;

public class ObjectReport {
	private String campo_report;
	private String mes;
	private Double data;
	
	public ObjectReport(String campo_report, String mes, Double data) {
		super();
		this.campo_report = campo_report;
		this.mes = mes;
		this.data = data;
	}
	public ObjectReport() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getCampo_report() {
		return campo_report;
	}
	public void setCampo_report(String campo_report) {
		this.campo_report = campo_report;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public Double getData() {
		return data;
	}
	public void setData(Double data) {
		this.data = data;
	}
	
}
