package com.sofipa.sifiso.app.repositories;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.sofipa.sifiso.app.models.EstadoDeResultados;

public interface EstadoDeResultadosDAO extends Repository<EstadoDeResultados, Long> {

	@Query("From EstadoDeResultados E where E.anio =:anio and E.mes=:mes and E.sucursal=:sucursal")
	EstadoDeResultados getEdoResult(@Param("anio") int anio, @Param("mes") int mes, @Param("sucursal") int sucursal);

}
