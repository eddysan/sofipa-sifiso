package com.sofipa.sifiso.app.services;

import java.util.List;
import com.sofipa.sifiso.app.models.Indicadores;

public interface IIndicadoresService {
	//Busqueda General
	public List<Indicadores> findAll();
	
	//Busqueda por region
	public List<Indicadores> findByAnioAndMesLessThanAndSucursalRegionId(int anio, int mes, Long region);
	public List<Indicadores> findByAnioAndMesAndSucursalRegionId(int anio, int mes, Long region);
	
	//Buesqueda por regiones
	public List<Indicadores> findByAnioAndMesLessThanAndSucursalId(int anio, int mes, Long sucursal);
	public Indicadores findByAnioAndMesAndSucursalId(int anio, int mes, Long sucursal);
}
