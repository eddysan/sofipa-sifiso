package com.sofipa.sifiso.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SofipaSifisoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SofipaSifisoApplication.class, args);
	}

}
