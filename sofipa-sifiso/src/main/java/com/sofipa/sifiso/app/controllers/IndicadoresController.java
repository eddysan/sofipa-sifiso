package com.sofipa.sifiso.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.sofipa.sifiso.app.models.Indicadores;
import com.sofipa.sifiso.app.services.IIndicadoresService;

import java.util.ArrayList;
import java.util.List;

@RestController
public class IndicadoresController {
	
	@Autowired
	private IIndicadoresService is;
	
	@GetMapping("/indicadores/region/{anio}/{mes}/{region}")
	public List<Indicadores> getIndicadoresRegion(@PathVariable("anio") int anio, @PathVariable("mes") int mes, @PathVariable("region") long region ) {
		List<Indicadores> indicadores = is.findByAnioAndMesLessThanAndSucursalRegionId(anio, mes+1, region);
		
		List<Indicadores> ind_return = new ArrayList<Indicadores>();
		ind_return.add(0,this.getIndicadoresRegionPorMes(anio-1, 12, region));
		for(int i = 0; i<(mes-1); i++) {
			Indicadores indAux = new Indicadores();
			int j = i;
			indAux.setNumero_clientes(indicadores.stream().filter(ind-> ind.getMes() == j ).mapToInt((indf) -> indf.getNumero_clientes()).sum());
			indAux.setSaldo_cartera(indicadores.stream().filter(ind-> ind.getMes() == j ).mapToDouble((indf) -> indf.getSaldo_cartera()).sum());
			indAux.setCredito_promedio(indicadores.stream().filter(ind-> ind.getMes() == j ).mapToDouble((indf) -> indf.getCredito_promedio()).sum());
			indAux.setClientes_promedio(indicadores.stream().filter(ind-> ind.getMes() == j ).mapToDouble((indf) -> indf.getClientes_promedio()).sum());
			indAux.setCartera_promedio(indicadores.stream().filter(ind-> ind.getMes() == j ).mapToDouble((indf) -> indf.getCartera_promedio()).sum());
			indAux.setPlantilla_total(indicadores.stream().filter(ind-> ind.getMes() == j ).mapToDouble((indf) -> indf.getPlantilla_total()).sum());
			indAux.setAnalista_credito(indicadores.stream().filter(ind-> ind.getMes() == j ).mapToDouble((indf) -> indf.getAnalista_credito()).sum());
			Double cartera_vencida = indicadores.stream().filter(ind-> ind.getMes() == j ).mapToDouble((indf) -> indf.getSaldo_cartera()*indf.getCarteravencida_percent()).sum();
			indAux.setCarteravencida_percent(cartera_vencida/indAux.getSaldo_cartera());
			indAux.setNormalidad(1-indAux.getCarteravencida_percent());
			indAux.setMes(j);
			indAux.setAnio(anio);
			ind_return.add(indAux);
		}
		return ind_return;
	}
	
	@GetMapping("/indicadores/region/mes/{anio}/{mes}/{region}")
	public Indicadores getIndicadoresRegionPorMes(@PathVariable("anio") int anio, @PathVariable("mes") int mes, @PathVariable("region") long region ){
		List<Indicadores> indicadores =  is.findByAnioAndMesAndSucursalRegionId(anio, mes, region);
		
		Indicadores indAux = new Indicadores();
		indAux.setNumero_clientes(indicadores.stream().filter(ind-> ind.getMes() == mes ).mapToInt((indf) -> indf.getNumero_clientes()).sum());
		indAux.setSaldo_cartera(indicadores.stream().filter(ind-> ind.getMes() == mes ).mapToDouble((indf) -> indf.getSaldo_cartera()).sum());
		indAux.setCredito_promedio(indicadores.stream().filter(ind-> ind.getMes() == mes ).mapToDouble((indf) -> indf.getCredito_promedio()).sum());
		indAux.setClientes_promedio(indicadores.stream().filter(ind-> ind.getMes() == mes ).mapToDouble((indf) -> indf.getClientes_promedio()).sum());
		indAux.setCartera_promedio(indicadores.stream().filter(ind-> ind.getMes() == mes ).mapToDouble((indf) -> indf.getCartera_promedio()).sum());
		indAux.setPlantilla_total(indicadores.stream().filter(ind-> ind.getMes() == mes ).mapToDouble((indf) -> indf.getPlantilla_total()).sum());
		indAux.setAnalista_credito(indicadores.stream().filter(ind-> ind.getMes() == mes ).mapToDouble((indf) -> indf.getAnalista_credito()).sum());
		Double cartera_vencida = indicadores.stream().filter(ind-> ind.getMes() == mes ).mapToDouble((indf) -> indf.getSaldo_cartera()*indf.getCarteravencida_percent()).sum();
		indAux.setCarteravencida_percent(cartera_vencida/indAux.getSaldo_cartera());
		indAux.setNormalidad(1-indAux.getCarteravencida_percent());
		indAux.setMes(0);
		indAux.setAnio(anio);
		
		return indAux;
		
	}
	
	@GetMapping("/indicadores/sucursal/{anio}/{mes}/{sucursal}")
	public List<Indicadores> getIndicadoresSucursal(@PathVariable("anio") int anio, @PathVariable("mes") int mes, @PathVariable("sucursal") long sucursal ){
		List<Indicadores> retrn = new ArrayList<Indicadores>();
		Indicadores dic = is.findByAnioAndMesAndSucursalId(anio-1, 12, sucursal);
		retrn = is.findByAnioAndMesLessThanAndSucursalId(anio, mes +1, sucursal);
		retrn.add(0, dic);
		retrn.get(0).setMes(0);
		return retrn;
	}
}
