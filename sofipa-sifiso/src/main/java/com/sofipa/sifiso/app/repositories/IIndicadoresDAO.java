package com.sofipa.sifiso.app.repositories;


import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.sofipa.sifiso.app.models.Indicadores;


public interface IIndicadoresDAO extends CrudRepository<Indicadores, Long>{	
	List<Indicadores> findAll();
	//Busqueda Por regiones
	List<Indicadores> findByAnioAndMesLessThanAndSucursalRegionId(int anio, int mes, Long region);
	List<Indicadores> findByAnioAndMesAndSucursalRegionId(int anio, int mes, Long region);
	
	//Busqueda por sucursal
	List<Indicadores> findByAnioAndMesLessThanAndSucursalId(int anio, int mes, Long sucursal);
	Indicadores findByAnioAndMesAndSucursalId(int anio, int mes, Long sucursal);
}
