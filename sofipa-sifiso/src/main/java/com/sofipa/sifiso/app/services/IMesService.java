package com.sofipa.sifiso.app.services;

import java.util.List;

import com.sofipa.sifiso.app.models.Mes;


public interface IMesService {
	public List<Mes> getMeses();
}
