package com.sofipa.sifiso.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.sofipa.sifiso.app.models.Prorrateo;


public interface IProrrateoDAO extends Repository<Prorrateo, Long> {
	
	@Query("FROM Prorrateo p WHERE p.anio = :anio and p.mes = :mes")
	public List<Prorrateo> getProrrateo(@Param("mes") int mes, @Param("anio") int anio);

	@Query("Select sum(p.cartera) From Prorrateo p where p.anio = :anio and p.mes = :mes")
	public Double getTotalCartera(@Param("mes") int mes, @Param("anio") int anio);
	
	@Query("Select p.cartera From Prorrateo p Where  p.anio = :anio and p.mes = :mes and p.sucursal = :sucursal")
	public Double getCartera(@Param("mes") int mes, @Param("anio") int anio, @Param("sucursal") int sucursal);
	
	@Query("Select sum(p.personal) From Prorrateo p where p.anio = :anio and p.mes = :mes")
	public Integer getTotalPersonal(@Param("mes") int mes, @Param("anio") int anio);
	
	@Query("Select p.personal From Prorrateo p Where  p.anio = :anio and p.mes = :mes and p.sucursal = :sucursal")
	public Integer getPersonal(@Param("mes") int mes, @Param("anio") int anio, @Param("sucursal") int sucursal);
	
	@Query("Select p.eprc From Prorrateo p Where p.anio = :anio and p.mes = :mes and p.sucursal = :sucursal")
	public Double getEstimacionPorRiesgosCrediticios(@Param("mes") int mes, @Param("anio") int anio, @Param("sucursal") int sucursal);
	
	@Query("Select sum(p.eprc) From Prorrateo p Where p.anio = :anio and p.mes = :mes and p.eprc > 0")
	public Double getEPRCConsolidado(@Param("mes") int mes, @Param("anio") int anio);
	
	@Query("Select abs(sum(p.eprc)) From Prorrateo p Where p.anio = :anio and p.mes = :mes and p.eprc < 0")
	public Double getEPRCConsolidado2(@Param("mes") int mes, @Param("anio") int anio);
	
	@Query("delete from Prorrateo p where p.anio = :anio and p.mes= :mes")
	@Modifying
    @Transactional
	void deleteProrrateo(@Param("anio") int anio, @Param("mes") int mes);
}
