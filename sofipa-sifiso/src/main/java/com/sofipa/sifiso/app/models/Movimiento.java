package com.sofipa.sifiso.app.models;


public class Movimiento {
	
	public class Cuenta {
		private  String nombre_cuenta;

		
		
		public Cuenta(String nombre_cuenta) {
			super();
			this.nombre_cuenta = nombre_cuenta;
		}

		public String getNombre_cuenta() {
			return nombre_cuenta;
		}

		public void setNombre_cuenta(String nombre_cuenta) {
			this.nombre_cuenta = nombre_cuenta;
		}
	}
	
	private String clave_cuenta;
	private Double saldo_inicial,cargo, abono,saldo_final;
	private Cuenta cuenta;
	
	public Movimiento(String clave_cuenta, Double saldo_inicial, Double cargo, Double abono, Double saldo_final,
			Cuenta cuenta) {
		super();
		this.clave_cuenta = clave_cuenta;
		this.saldo_inicial = saldo_inicial;
		this.cargo = cargo;
		this.abono = abono;
		this.saldo_final = saldo_final;
		this.cuenta = cuenta;
	}

	public String getClave_cuenta() {
		return clave_cuenta;
	}

	public void setClave_cuenta(String clave_cuenta) {
		this.clave_cuenta = clave_cuenta;
	}

	public Double getSaldo_inicial() {
		return saldo_inicial;
	}

	public void setSaldo_inicial(Double saldo_inicial) {
		this.saldo_inicial = saldo_inicial;
	}

	public Double getCargo() {
		return cargo;
	}

	public void setCargo(Double cargo) {
		this.cargo = cargo;
	}

	public Double getAbono() {
		return abono;
	}

	public void setAbono(Double abono) {
		this.abono = abono;
	}

	public Double getSaldo_final() {
		return saldo_final;
	}

	public void setSaldo_final(Double saldo_final) {
		this.saldo_final = saldo_final;
	}

	public Cuenta getCuenta() {
		return cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}
	
	
	
	
}
