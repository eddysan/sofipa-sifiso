package com.sofipa.sifiso.app.repositories;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Repository;

import com.sofipa.sifiso.app.models.ObjectReport;

import org.springframework.beans.factory.annotation.Autowired;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

@Repository
public class DaoReport {
	
	@Autowired
	private ResourceLoader resourceLoader;
	
	public JasperPrint exportPDF(String sucursal, int anio, String mes, List<ObjectReport> c, List<ObjectReport> i, List<ObjectReport> g)throws JRException, IOException{
		String path = resourceLoader.getResource("classpath:Blank_A4_Landscape.jrxml").getURI().getPath();
		JasperReport jasperReport = JasperCompileManager.compileReport(path);
		// Parameters for report
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("sucursal", sucursal);
		parameters.put("anio", 2017 + anio);
		parameters.put("mes", mes);
		parameters.put("resultados",c);
		parameters.put("indicadores", i);
		parameters.put("grafica", g);
		JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters,new JREmptyDataSource());
		return print;
	}
}
