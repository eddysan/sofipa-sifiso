package com.sofipa.sifiso.app.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tabla_cuenta_mapeo")
public class GrupoCuenta implements Serializable{
	private static final long serialVersionUID = 1L;
	

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_cuenta_mapeo")
	private long id_cuenta_mapeo;
	private String nombre;
	

	public GrupoCuenta(long id_cuenta_mapeo, String nombre) {
		super();
		this.id_cuenta_mapeo = id_cuenta_mapeo;
		this.nombre = nombre;
	}

	public GrupoCuenta() {
		super();
	}

	public long getId_cuenta_mapeo() {
		return id_cuenta_mapeo;
	}

	public void setId_cuenta_mapeo(long id_cuenta_mapeo) {
		this.id_cuenta_mapeo = id_cuenta_mapeo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	} 
}
