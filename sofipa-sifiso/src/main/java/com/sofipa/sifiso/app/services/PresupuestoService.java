package com.sofipa.sifiso.app.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sofipa.sifiso.app.models.GrupoCuenta;
import com.sofipa.sifiso.app.models.Presupuesto;
import com.sofipa.sifiso.app.models.PresupuestoAux;
import com.sofipa.sifiso.app.models.PresupuestoAux.GCuenta;
import com.sofipa.sifiso.app.repositories.IGrupoCuentaDAO;
import com.sofipa.sifiso.app.repositories.IPresupuestoDAO;
import com.sofipa.sifiso.app.repositories.IPresupuestoDAOR;

@Service
public class PresupuestoService implements IPresupuestoService {
	
	@Autowired
	private IPresupuestoDAO presupuesto;
	
	@Autowired
	private IGrupoCuentaDAO grupo;
	
	@Autowired 
	private IPresupuestoDAOR pr;
	
	@Override
	public List<Presupuesto> getPresupuesto(int anio, int mes, int sucursal, long grupo) {
		return presupuesto.getPresupuesto(anio, mes, sucursal, grupo);
	}

	@Override
	@Transactional
	public void setPresupuesto(PresupuestoAux pres) {
		for(GCuenta c: pres.getCuentas()) {
			GrupoCuenta cuenta = grupo.getGrupo(c.nombre);
			if(cuenta != null) {
				Presupuesto mov = new Presupuesto();
				mov.setAnio(pres.getAnio());
				mov.setMes(pres.getMes());
				mov.setSucursal(pres.getSucursal());
				mov.setMonto(c.monto);
				mov.setCuenta(cuenta);
				pr.save(mov);
			}		
		}
	}

	@Override
	public List<Presupuesto> getPresupuesto(int anio, int mes, int sucursal) {
		// TODO Auto-generated method stub
		return presupuesto.getPresupuesto(anio, mes, sucursal);
	}

}