package com.sofipa.sifiso.app.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tabla_prorrateo")
public class Prorrateo {
	
	private static final long serialversionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_prorrateo")
	private Long id_prorrateo;
	private int mes;
	private int anio;
	private int sucursal;
	private Double eprc;
	private int personal;
	private int personal_analista; 
	private Double cartera;
	public int getPersonal_analista() {
		return personal_analista;
	}


	public void setPersonal_analista(int personal_analista) {
		this.personal_analista = personal_analista;
	}


	public Double getCartera_vencida() {
		return cartera_vencida;
	}


	public void setCartera_vencida(Double cartera_vencida) {
		this.cartera_vencida = cartera_vencida;
	}


	public int getNumero_clientes() {
		return numero_clientes;
	}


	public void setNumero_clientes(int numero_clientes) {
		this.numero_clientes = numero_clientes;
	}
	private Double cartera_vencida;
	private int numero_clientes;	
	
	
	
	
	
	
	public Prorrateo(Long id_prorrateo, int mes, int anio, int sucursal, Double eprc, int personal,
			int personal_analista, Double cartera, Double cartera_vencida, int numero_clientes) {
		super();
		this.id_prorrateo = id_prorrateo;
		this.mes = mes;
		this.anio = anio;
		this.sucursal = sucursal;
		this.eprc = eprc;
		this.personal = personal;
		this.personal_analista = personal_analista;
		this.cartera = cartera;
		this.cartera_vencida = cartera_vencida;
		this.numero_clientes = numero_clientes;
	}


	public Prorrateo() {
		super();
	}

	public Double getEprc() {
		return eprc;
	}



	public void setEprc(Double eprc) {
		this.eprc = eprc;
	}

	public Long getId_prorrateo() {
		return id_prorrateo;
	}
	public void setId_prorrateo(Long id_prorrateo) {
		this.id_prorrateo = id_prorrateo;
	}
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public int getAnio() {
		return anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
	public int getSucursal() {
		return sucursal;
	}
	public void setSucursal(int sucursal) {
		this.sucursal = sucursal;
	}
	public int getPersonal() {
		return personal;
	}
	public void setPersonal(int personal) {
		this.personal = personal;
	}
	public Double getCartera() {
		return cartera;
	}
	public void setCartera(Double cartera) {
		this.cartera = cartera;
	}
	public static long getSerialversionuid() {
		return serialversionUID;
	}
	
}
