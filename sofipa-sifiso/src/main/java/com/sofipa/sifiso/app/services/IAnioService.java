package com.sofipa.sifiso.app.services;

import java.util.List;

import com.sofipa.sifiso.app.models.Anio;


public interface IAnioService {
	public List<Anio> getAnios();
}
