package com.sofipa.sifiso.app.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sofipa.sifiso.app.models.Anio;

public interface IAnioDAO extends CrudRepository<Anio, Long>{
	
}
