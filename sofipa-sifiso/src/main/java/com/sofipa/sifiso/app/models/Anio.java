package com.sofipa.sifiso.app.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tabla_anio")
public class Anio {
	private static final long serialversionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_anio")
	private int id;
	private int anio;
	
	public Anio(int id, int anio) {
		super();
		this.id = id;
		this.anio = anio;
	}
	
	public Anio() {
		super();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAnio() {
		return anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
	public static long getSerialversionuid() {
		return serialversionUID;
	}
	
	
}
