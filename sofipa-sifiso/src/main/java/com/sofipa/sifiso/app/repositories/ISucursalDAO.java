package com.sofipa.sifiso.app.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import com.sofipa.sifiso.app.models.Sucursal;
import java.util.List;

public interface ISucursalDAO extends Repository<Sucursal, Long>{
	@Query("From Sucursal s where s.estatus = 'A'")
	public List<Sucursal> getSucursales();
	
	@Query("From Sucursal s where s.region.id = :region order by s.id DESC")
	public List<Sucursal> getSucursalesRegion(@Param("region") long region);
}
