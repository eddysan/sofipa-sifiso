package com.sofipa.sifiso.app.services;

import java.util.List;

import com.sofipa.sifiso.app.models.Balanza;


public interface IBalanzaService {
	public List<Balanza> getBalanza(int sucursal, int anio, int mes);
	public boolean setNewBalanza(int anio, int mes, int sucursal, String movs);
	
	public Double getIngresosPorIntereses(int mes_act, int anio_act);
	public Double getGastosPorIntereses(int anio_act, int mes_act);
	public List<Double> getIngresosAnio(int anio);
	public List<Double> getGastosAnio(int anio);
	public Double getBalanzaPorCaratula(int sucursl, int anio, int mes, String cuenta);
	public Double getBalanzaPorCaratula2(int anio, int mes);
	public List<Balanza> findDataProrrateo (int mes, int anio);
	public Double getTotalGastosInteres(int mes, int anio);
	public void deleteBalanza (int anio, int mes, int sucursal);
}
